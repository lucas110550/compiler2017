package Front.Expression;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.Operand;
import Front.Node;
import Front.Type.Type;

import java.util.*;

public abstract class Expression implements Node {
    public Type type;
    public boolean mark;
    public Operand operand;

    public Expression(Type type, boolean mark) {
        this.type = type;
        this.mark = mark;
    }
    public abstract void combine(List<Instruction> instr);
    public void load(List<Instruction> instr) {}
}