package Front.Expression.VariableExpression;

import ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import ControlFlowGraph.Operand.Address;
import ControlFlowGraph.Operand.ImmediateValue;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Enviroment.Enviroment;
import Front.Expression.Expression;
import Front.Type.ArrayType;
import Front.Type.BasicType.StringType;
import Front.Type.ClassType.*;
import Front.Type.Type;
import exception.CompileError;
import exception.InternalError;
import Utility.Util;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class FieldExpression extends Expression {
    public Expression expression;
    public String field;
    public FieldExpression(Type type, boolean mark, Expression expression, String field) {
        super(type, mark);
        this.expression = expression;
        this.field = field;
    }
    public static Expression getExpression(Expression expression, String other) {
        if (expression.type instanceof ClassType) {
            ClassType classType = (ClassType)expression.type;
            Member member = classType.getMember(other);
            if (member instanceof MemberVariable)
                return new FieldExpression(((MemberVariable)member).type, expression.mark, expression, other);
            if (member instanceof MemberFunction)
                return new FieldExpression(((MemberFunction)member).function, expression.mark, expression, other);
            throw new InternalError();
        }
        else if (expression.type instanceof ArrayType) {
            if (other.equals("size"))
                return new FieldExpression(Enviroment.symboltable.find("___array_size").type, expression.mark, expression, other);
        }
        else if (expression.type instanceof StringType) {
            if (other.equals("length")) {
                return new FieldExpression(Enviroment.symboltable.find("___string_length").type, expression.mark, expression, other);
            } else if (other.equals("ord")) {
                return new FieldExpression(Enviroment.symboltable.find("___string_ord").type, expression.mark, expression, other);
            } else if (other.equals("substring")) {
                return new FieldExpression(Enviroment.symboltable.find("___string_substring").type, expression.mark, expression, other);
            } else if (other.equals("parseInt")) {
                return new FieldExpression(Enviroment.symboltable.find("___string_parseInt").type, expression.mark, expression, other);
            }
        }
        throw new CompileError("The field Function does not match the array or class.");
    }
    @Override
    public String toString() {
        return "Expression: field";
    }

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        str.append(expression.toString(indents + 1));
        str.append(Util.getindent(indents + 1)).append("field name = ").append(field).append(", type = ").append(type.toString()).append("\n");
        return str.toString();
    }
    @Override
    public void load(List<Instruction> instr) {
        if (operand instanceof Address) {
            Address address = (Address) operand;
            operand = Enviroment.registertable.addTempRegister();
            instr.add(LoadInstruction.getInstruction(operand, address));
        }
    }

    @Override
    public void combine(List<Instruction> instr) {
        if (expression.type instanceof ClassType) {
            ClassType classType = (ClassType)expression.type;
            Member member = classType.getMember(field);
            if (member instanceof MemberVariable) {
                MemberVariable memberVariable = (MemberVariable)member;
                expression.combine(instr);
                expression.load(instr);
                VirtualRegister base = (VirtualRegister)expression.operand;
                ImmediateValue index = new ImmediateValue(memberVariable.offset);
                operand = new Address(base, index, memberVariable.type.size());
            }
        }
    }
}