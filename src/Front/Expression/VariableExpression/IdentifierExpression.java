package Front.Expression.VariableExpression;

import Enviroment.Enviroment;
import Symbol.Symbol;
import Front.Expression.Expression;

import Front.Function;
import Front.Type.ClassType.ClassType;
import Front.Type.Type;
import exception.CompileError;
import Utility.Util;

import java.util.*;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class IdentifierExpression extends Expression {
    public Symbol symbol;
    public IdentifierExpression(Type returnType, boolean mark, Symbol symbol) {
        super(returnType, mark);
        this.symbol = symbol;
    }
    public static Expression getExpression(String name) {
        if (!Enviroment.symboltable.contains(name))
            throw new CompileError("Do not have this symbol name.");
        Symbol symbol = Enviroment.symboltable.find(name);
        if (symbol.scope instanceof ClassType)
            return FieldExpression.getExpression(IdentifierExpression.getExpression("this"), name);
        else {
            if (symbol.type instanceof Function)
                return new IdentifierExpression(symbol.type, false, symbol);
            else return new IdentifierExpression(symbol.type, true, symbol);
        }
    }
    @Override
    public String toString() {
        return "Expression: Identifier: name = " + symbol.name + ", type = " + type;
    }

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents));
        str.append(toString()).append("\n");
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        operand = symbol.register;
    }
}