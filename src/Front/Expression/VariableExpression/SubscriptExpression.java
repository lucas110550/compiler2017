package Front.Expression.VariableExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.AdditionInstruction;
import ControlFlowGraph.Instruction.BinaryInstruction.MultiplyInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import ControlFlowGraph.Operand.Address;
import ControlFlowGraph.Operand.ImmediateValue;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Enviroment.Enviroment;
import Front.Expression.BinaryExpression.MultiplyExpression;
import Front.Expression.Expression;
import Front.Type.ArrayType;
import Front.Type.BasicType.*;
import Front.Type.Type;
import exception.CompileError;
import Utility.Util;

import java.util.*;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class SubscriptExpression extends Expression {
    public Expression expression, subscript;

    public SubscriptExpression(Type type, boolean mark, Expression expression, Expression subscript) {
        super(type, mark);
        this.expression = expression;
        this.subscript = subscript;
    }

    public static Expression getExpression(Expression expression, Expression subscript) {
        if ((expression.type instanceof ArrayType) == false)
            throw new CompileError("You should use a array type to make subscript expression.");
        if ((subscript.type instanceof IntType) == false)
            throw new CompileError("You should ues a int type to make subscript expression.");
        ArrayType tmp = (ArrayType)expression.type;
        return new SubscriptExpression(tmp.reduce(), expression.mark, expression, subscript);
    }
    @Override
    public String toString() {
        return "Expression: SubscriptExpression";
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(this).append("\n");
        str.append(expression.toString(indents + 1));
        str.append(subscript.toString(indents + 1));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        expression.combine(instr);
        expression.load(instr);
        subscript.combine(instr);
        subscript.load(instr);
        VirtualRegister address = Enviroment.registertable.addTempRegister();
        VirtualRegister disp = Enviroment.registertable.addTempRegister();
        instr.add(MultiplyInstruction.getInstruction(disp, subscript.operand, new ImmediateValue(type.size())));
        instr.add(AdditionInstruction.getInstruction(address, expression.operand, disp));
        operand = new Address(address, type.size());
    }
    @Override
    public void load(List<Instruction> instr) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Enviroment.registertable.addTempRegister();
            instr.add(LoadInstruction.getInstruction(operand, address));
        }
    }
}