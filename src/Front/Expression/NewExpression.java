package Front.Expression;

import ControlFlowGraph.Instruction.BinaryInstruction.*;
import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.AllocateInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import ControlFlowGraph.Operand.Address;
import ControlFlowGraph.Operand.ImmediateValue;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Enviroment.Enviroment;
import Front.Function;
import Front.Type.ArrayType;
import Front.Type.BasicType.IntType;
import Front.Type.ClassType.ClassType;
import Front.Type.Type;
import com.sun.org.apache.xpath.internal.operations.Minus;
import exception.CompileError;
import exception.InternalError;
import Utility.Util;

import java.util.*;

public class NewExpression extends Expression {
    public List<Expression> expressions;
    public Function constructor;
    public NewExpression(Type type, boolean mark, List<Expression> expressions) {
        super(type, mark);
        this.expressions = expressions;
        this.constructor = null;
    }
    public static Expression getExpression(Type type, List<Expression> expressions) {
        if (expressions.size() == 0) {
            if (type instanceof ClassType) {
                return new NewExpression(type, false, expressions);
            }
            else throw new CompileError("You need a array type or class type to make dimensions.");
        }
        else {
            Type tmp = ArrayType.getType(type, expressions.size(), expressions);
            return new NewExpression(tmp, false, expressions);
        }
    }
    @Override
    public String toString() {
        return "Expression: new, type = " + type;
    }

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(this).append("\n");
        expressions.stream()
                .filter(expression -> expression != null)
                .forEach(expression -> str.append(expression.toString(indents + 1)));
        if (constructor != null) {
            str.append(Util.getindent(indents)).append("[constructor]").append("\n");
        }
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instructions) {
        for (Expression expression : expressions) {
            if (expression == null) continue;
            expression.combine(instructions);
            expression.load(instructions);
        }
        if (type instanceof ClassType) buildclass(type, instructions);
        else if (type instanceof ArrayType) operand = buildArray((ArrayType)type, 0, instructions);
        else throw new InternalError();
    }
    private Operand buildclass(Type type, List<Instruction> instructions) {
        operand = Enviroment.registertable.addTempRegister();
        if (type instanceof ClassType) {
            ClassType classType = (ClassType)type;
            instructions.add(AllocateInstruction.getInstruction(operand, new ImmediateValue(classType.Size)));
            classType.memberVariables.forEach((name, member) -> {
                Address address = new Address((VirtualRegister)operand, new ImmediateValue(member.offset), member.type.size());
                if (member.expression != null) {
                    member.expression.combine(instructions);
                    member.expression.load(instructions);
                    instructions.add(StoreInstruction.getInstruction(member.expression.operand, address));
                }
            });
            if (constructor == null) {
                if (!classType.constructors.isEmpty()) {
                    List<Type> x = new ArrayList<Type>();
                    x.add(classType);
                    if (!classType.constructors.containsKey(x))
                        throw new CompileError("This class has no constructor.");
                    constructor = classType.constructors.get(x);
                }
            }
            if (constructor != null) {
                List<Operand> x = new ArrayList<Operand>();
                x.add(operand);
                instructions.add(CallInstruction.getInstruction(null, constructor, x));
            }
        }
        return operand;
    }
    private Operand buildArray(ArrayType x, int y, List<Instruction> instructions) {
        VirtualRegister ss = Enviroment.registertable.addTempRegister();
        VirtualRegister dd = Enviroment.registertable.addTempRegister();
        VirtualRegister xx = Enviroment.registertable.addTempRegister();
        VirtualRegister yy = Enviroment.registertable.addTempRegister();
        if (y >= x.expressions.size()) return dd;
        if (x.expressions.get(y) == null) return dd;
        instructions.add(MovInstruction.getInstruction(ss, x.expressions.get(y).operand));
        instructions.add(AdditionInstruction.getInstruction(ss, ss, new ImmediateValue(1)));
        instructions.add(MultiplyInstruction.getInstruction(ss, ss, new ImmediateValue(8)));
        instructions.add(AllocateInstruction.getInstruction(dd, ss));
        instructions.add(MovInstruction.getInstruction(ss, x.expressions.get(y).operand));
        instructions.add(StoreInstruction.getInstruction(ss, new Address(dd, new ImmediateValue(0), 8)));
        instructions.add(AdditionInstruction.getInstruction(dd, dd, new ImmediateValue(8)));
        int flag = 0;
        if (x.baseType instanceof ClassType && y + 1 == x.expressions.size()) flag = 1;
        if (y + 1 < x.expressions.size()) flag = 2;
        if (flag != 0) {
            instructions.add(MovInstruction.getInstruction(ss, x.expressions.get(y).operand));
            instructions.add(MinusInstruction.getInstruction(ss, ss, new ImmediateValue(1)));
            instructions.add(MultiplyInstruction.getInstruction(ss, ss, new ImmediateValue(8)));
            instructions.add(AdditionInstruction.getInstruction(ss, ss, dd));
            LabelInstruction loop = LabelInstruction.getInstruction("loop");
            LabelInstruction subStatement = LabelInstruction.getInstruction("subStatement");
            LabelInstruction all = LabelInstruction.getInstruction("all");
            instructions.add(JumpInstruction.getInstruction(loop));
            instructions.add(loop);
            instructions.add(GEQInstruction.getInstruction(yy, ss, dd));
            instructions.add(BranchInstruction.getInstruction(yy, subStatement, all));
            instructions.add(subStatement);
            if (flag == 1) {
                instructions.add(MovInstruction.getInstruction(xx, buildclass(x.baseType, instructions)));
                instructions.add(StoreInstruction.getInstruction(xx, new Address(ss, new ImmediateValue(0), 8)));
            }
            else {
                instructions.add(MovInstruction.getInstruction(xx, buildArray(x, y + 1, instructions)));
                instructions.add(StoreInstruction.getInstruction(xx, new Address(ss, new ImmediateValue(0), 8)));
            }
            instructions.add(MinusInstruction.getInstruction(ss, ss, new ImmediateValue(8)));
            instructions.add(JumpInstruction.getInstruction(loop));
            instructions.add(all);

        }
        return dd;
    }
}
