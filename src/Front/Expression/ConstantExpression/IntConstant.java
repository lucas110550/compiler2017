package Front.Expression.ConstantExpression;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.ImmediateValue;
import Front.Type.BasicType.IntType;

import java.util.*;
public class IntConstant extends Constant {
    public int value;
    public IntConstant(int value) {
        super(IntType.getType());
        this.value = value;
    }
    public static Constant getConstant(int value) {
        return new IntConstant(value);
    }
    @Override
    public String toString() {
        return "Constant : int, value = " + value;
    }
    @Override
    public void combine(List<Instruction> instr) {
        operand = new ImmediateValue(value);
    }
}