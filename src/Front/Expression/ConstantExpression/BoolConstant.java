package Front.Expression.ConstantExpression;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.ImmediateValue;
import Front.Type.BasicType.BoolType;
import java.util.*;
public class BoolConstant extends Constant {
    public boolean value;
    public BoolConstant(boolean value) {
        super(BoolType.getType());
        this.value = value;
    }
    public static Constant getConstant(boolean value) {
        return new BoolConstant(value);
    }
    @Override
    public String toString() {
        return "Constant : bool, value = " + value;
    }
    @Override
    public void combine(List<Instruction> instr) {
        operand = new ImmediateValue(value ? 1 : 0);
    }
}