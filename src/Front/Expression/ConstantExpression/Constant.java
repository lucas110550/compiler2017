package Front.Expression.ConstantExpression;

import Front.Expression.Expression;
import Front.Type.Type;
import Utility.Util;

public abstract class Constant extends Expression {
    public Constant(Type type) {
        super(type, false);
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent((indents))).append(toString()).append("\n");
        return str.toString();
    }
}