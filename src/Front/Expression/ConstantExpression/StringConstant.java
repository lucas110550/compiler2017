package Front.Expression.ConstantExpression;

import ControlFlowGraph.Instruction.Instruction;
import Front.Type.BasicType.StringType;
import Enviroment.Enviroment;

import java.util.*;
public class StringConstant extends Constant {
    public String value;
    public StringConstant(String value) {
        super(StringType.getType());
        this.value = value;
    }
    public static Constant getConstant(String value) {
        return new StringConstant(value);
    }
    @Override
    public String toString() {
        return "Constant : String, value = " + value;
    }
    @Override
    public void combine(List<Instruction> instr) {
        operand = Enviroment.registertable.addStringRegister(value);
    }
}