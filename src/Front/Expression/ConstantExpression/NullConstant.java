package Front.Expression.ConstantExpression;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.ImmediateValue;
import Front.Type.BasicType.NullType;

import java.util.*;
public class NullConstant extends Constant {
    public NullConstant() {
        super(NullType.getType());
    }
    public static Constant getConstant() {
        return new NullConstant();
    }
    @Override
    public String toString() {
        return "Constant : null";
    }
    @Override
    public void combine(List<Instruction> instr) {
        operand = new ImmediateValue(0);
    }
}