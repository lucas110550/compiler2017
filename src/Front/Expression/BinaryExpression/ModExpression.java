package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.ModInstruction;
import Front.Expression.ConstantExpression.IntConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class ModExpression extends BinaryExpression {
	public ModExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}

	public static Expression getExpression(Expression l, Expression r) {
		if (l.type instanceof IntType && r.type instanceof IntType) {
			if (l instanceof IntConstant && r instanceof IntConstant)
				return IntConstant.getConstant((((IntConstant)l).value) % (((IntConstant)r).value));
			else 
			return new ModExpression(IntType.getType(), false, l, r);
		}
		throw new CompileError("You should use two Int type to do Mod.");
	}

	@Override
	public String toString() {
		return "Expression: Mod";
	}
	@Override
	public void combine(List<Instruction> instr) {
		l.combine(instr);
		l.load(instr);
		r.combine(instr);
		r.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(ModInstruction.getInstruction(operand, l.operand, r.operand));
	}
}
