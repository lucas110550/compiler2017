package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import ControlFlowGraph.Operand.Address;
import Enviroment.Enviroment;
import Front.Expression.Expression;
import Front.Type.Type;
import ControlFlowGraph.Instruction.Instruction;
import exception.CompileError;
import java.util.*;

public class AssignmentExpression extends BinaryExpression {
	public AssignmentExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}
	public static Expression getExpression(Expression l, Expression r) {
		if (l.mark == false) throw new CompileError("The Assignment should have a left-value on the left.");
		if (l.type.comparable(r.type) == false) throw new CompileError("The Assignment should have two same types.");
		return new AssignmentExpression(l.type, true, l, r);
	}
	@Override
	public String toString() {
		return "Expression: Assignment";
	}
	@Override
	public void combine(List<Instruction> instr) {
		l.combine(instr);
		r.combine(instr);
		r.load(instr);
		operand = l.operand;

		if (l.operand instanceof Address)
			instr.add(StoreInstruction.getInstruction(r.operand, l.operand));
		else instr.add(MovInstruction.getInstruction(l.operand, r.operand));
	}
	@Override
	public void load(List<Instruction> instr) {
		if (operand instanceof Address) {
			Address address = (Address)operand;
			operand = Enviroment.registertable.addTempRegister();
			instr.add(LoadInstruction.getInstruction(operand, address));
		}
	}
}
