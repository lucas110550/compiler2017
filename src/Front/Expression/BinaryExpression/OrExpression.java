package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import Enviroment.Enviroment;
import Front.Expression.ConstantExpression.BoolConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.BoolType;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class OrExpression extends BinaryExpression {
	private OrExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}
	public static Expression getExpression(Expression l, Expression r) {
		if (l.type instanceof BoolType && r.type instanceof BoolType) {
			if (l instanceof BoolConstant && r instanceof BoolConstant)
				return BoolConstant.getConstant(((BoolConstant)l).value || ((BoolConstant)r).value);
			return new OrExpression(BoolType.getType(), false, l, r);
		}
		throw new CompileError("You should compare two bool type under Or operation");
	}

	@Override
	public String toString() {
		return "expression: LogicOr";
	}

	@Override
	public void combine(List<Instruction> instr) {
		operand = Enviroment.registertable.addTempRegister();
		l.combine(instr);
		l.load(instr);

		LabelInstruction TrueCond = LabelInstruction.getInstruction("true_condition");
		LabelInstruction FalseCond = LabelInstruction.getInstruction("false_condition");
		LabelInstruction FullCond = LabelInstruction.getInstruction("full_condition");

		instr.add(BranchInstruction.getInstruction(l.operand, TrueCond, FalseCond));

		instr.add(FalseCond);
		r.combine(instr);
		r.load(instr);
		operand = r.operand;
		instr.add(JumpInstruction.getInstruction(FullCond));
		instr.add(TrueCond);
		instr.add(MovInstruction.getInstruction(operand, new ImmediateValue(1)));
		instr.add(JumpInstruction.getInstruction(FullCond));
		instr.add(FullCond);
	}
}
