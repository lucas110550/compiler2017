package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.BitXorInstruction;
import Enviroment.Enviroment;
import Front.Expression.ConstantExpression.IntConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import ControlFlowGraph.Instruction.Instruction;
import java.util.*;
public class BitXorExpression extends BinaryExpression {
	public BitXorExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}

	public static Expression getExpression(Expression l, Expression r) {
		if (l.type instanceof IntType && r.type instanceof IntType) {
			if (l instanceof IntConstant && r instanceof IntConstant)
				return IntConstant.getConstant((((IntConstant)l).value) ^ (((IntConstant)r).value));
			else 
			return new BitXorExpression(IntType.getType(), false, l, r);
		}
		throw new CompileError("You should use two Int type to do bit_xor.");
	}

	@Override
	public String toString() {
		return "Expression: bit_xor";
	}
	@Override
	public void combine(List<Instruction> instr) {
		l.combine(instr);
		l.load(instr);
		r.combine(instr);
		r.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(BitXorInstruction.getInstruction(operand, l.operand, r.operand));
	}
}
