package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.GEQInstruction;
import Enviroment.Enviroment;
import Front.Expression.ConstantExpression.*;
import Front.Expression.Expression;
import Front.Expression.FunctionCallExpression;
import Front.Function;
import Front.Type.BasicType.*;
import Front.Type.Type;
import exception.CompileError;

import java.util.*;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class GEQExpression extends BinaryExpression {
	public GEQExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}

	public static Expression getExpression(Expression l, Expression r) {
		if (l.type instanceof IntType && r.type instanceof IntType) {
			if (l instanceof IntConstant && r instanceof IntConstant)
				return BoolConstant.getConstant(((IntConstant)l).value >= ((IntConstant)r).value);
			else return new GEQExpression(BoolType.getType(), false, l, r);
		}
		else if (l.type instanceof StringType && r.type instanceof StringType) {
				if (l instanceof StringConstant && r instanceof StringConstant)
					return BoolConstant.getConstant(((StringConstant)l).value.compareTo(((StringConstant)r).value) >= 0);
				else return FunctionCallExpression.getExpression((Function)Enviroment.symboltable.find("___string_geq").type, new ArrayList<Expression>() {{ add(l); add(r); }});
		}
		throw new CompileError("You should compare two int or two string in GEQ operation");
	}
	@Override
	public String toString() {
		return "Expression: GreaterThanOrEqualTo";
	}
	@Override
	public void combine(List<Instruction> instr) {
		l.combine(instr);
		l.load(instr);
		r.combine(instr);
		r.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(GEQInstruction.getInstruction(operand, l.operand, r.operand));
	}
}
