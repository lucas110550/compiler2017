package Front.Expression.BinaryExpression;

import Front.Expression.Expression;
import Front.Type.Type;
import Utility.Util;

public abstract class BinaryExpression extends Expression {
    public Expression l, r;
    public BinaryExpression(Type type, boolean mark, Expression l, Expression r) {
        super(type, mark);
        this.l = l;
        this.r = r;
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents + 1)).append(toString()).append("\n");
        str.append(l.toString(indents + 1));
        str.append(r.toString(indents + 1));
        return str.toString();
    }
}