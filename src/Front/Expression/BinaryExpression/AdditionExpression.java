package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.AdditionInstruction;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
import Front.Expression.ConstantExpression.*;
import Front.Expression.Expression;
import Front.Expression.FunctionCallExpression;
import Front.Function;
import Front.Type.BasicType.*;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;

public class AdditionExpression extends BinaryExpression {
    public AdditionExpression(Type type, boolean mark, Expression l, Expression r) {
        super(type, mark, l, r);
    }
    public static Expression getExpression(Expression l, Expression r) {
        if (l.type instanceof IntType && r.type instanceof IntType) {
            if (l instanceof IntConstant && r instanceof IntConstant) {
                return IntConstant.getConstant(((IntConstant)l).value + ((IntConstant)r).value);
            }
            return new AdditionExpression(IntType.getType(), false, l, r);
        }
        else if (l.type instanceof StringType && r.type instanceof StringType) {
            if (l instanceof StringConstant && r instanceof StringConstant) {
                return StringConstant.getConstant(((StringConstant)l).value + ((StringConstant)r).value);
            }
          //  System.out.printf("fuck!\n");
            return FunctionCallExpression.getExpression((Function)Enviroment.symboltable.find("___string_concatenate").type, new ArrayList<Expression>() {{ add(l); add(r); }});
        }
        throw new CompileError("You should use two int / string type in addition");
    }
    @Override
    public String toString() {
        return "Expression: Addition";
    }
    @Override
    public void combine(List<Instruction> instr) {
        l.combine(instr);
        l.load(instr);
        r.combine(instr);
        r.load(instr);
        operand = Enviroment.registertable.addTempRegister();
        instr.add(AdditionInstruction.getInstruction(operand, l.operand, r.operand));
    }
}