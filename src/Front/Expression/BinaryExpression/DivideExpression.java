package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.DivideInstruction;
import Enviroment.Enviroment;
import Front.Expression.ConstantExpression.IntConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import ControlFlowGraph.Instruction.Instruction;
import java.util.*;
public class DivideExpression extends BinaryExpression {
	public DivideExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}

	public static Expression getExpression(Expression l, Expression r) {
		if (l.type instanceof IntType && r.type instanceof IntType) {
			if (l instanceof IntConstant && r instanceof IntConstant)
				return IntConstant.getConstant((((IntConstant)l).value) / (((IntConstant)r).value));
			else 
			return new DivideExpression(IntType.getType(), false, l, r);
		}
		throw new CompileError("You should use two Int type to do Divide.");
	}

	@Override
	public String toString() {
		return "Expression: Divide";
	}
	@Override
	public void combine(List<Instruction> instr) {
		l.combine(instr);
		l.load(instr);
		r.combine(instr);
		r.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(DivideInstruction.getInstruction(operand, l.operand, r.operand));
	}
}
