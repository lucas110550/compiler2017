package Front.Expression.BinaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.EqualToInstruction;
import Enviroment.Enviroment;
import Front.Expression.ConstantExpression.*;
import Front.Expression.Expression;
import Front.Expression.FunctionCallExpression;
import Front.Function;
import Front.Type.BasicType.*;
import Front.Type.Type;
import exception.CompileError;

import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import java.util.*;
public class EqualToExpression extends BinaryExpression {
	public EqualToExpression(Type type, boolean mark, Expression l, Expression r) {
		super(type, mark, l, r);
	}
	public static Expression getExpression(Expression l, Expression r) {
		if (!l.type.comparable(r.type))
			throw new CompileError("You should compare two has the same type under Equal.");
		if (l instanceof NullConstant && r instanceof NullConstant)
			return BoolConstant.getConstant(true);
		if (l instanceof BoolConstant && r instanceof BoolConstant)
			return BoolConstant.getConstant(((BoolConstant)l).value == ((BoolConstant)r).value);
		if (l instanceof IntConstant && r instanceof IntConstant)
			return BoolConstant.getConstant(((IntConstant)l).value == ((IntConstant)r).value);
		if (l instanceof StringConstant && r instanceof StringConstant)
			return BoolConstant.getConstant(((StringConstant)l).value.equals(((StringConstant)r).value));
		if (l.type instanceof StringType && r.type instanceof StringType)
			return FunctionCallExpression.getExpression((Function)Enviroment.symboltable.find("___string_equalto").type, new ArrayList<Expression>() {{ add(l); add(r); }});
		return new EqualToExpression(BoolType.getType(), false, l, r);
	}
	@Override
	public String toString() {
		return "Expression: Equalto";
	}
	@Override
	public void combine(List<Instruction> instr) {
		l.combine(instr);
		l.load(instr);
		r.combine(instr);
		r.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(EqualToInstruction.getInstruction(operand, l.operand, r.operand));
	}
}
