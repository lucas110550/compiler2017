package Front.Expression;

import ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.Operand;
import Enviroment.Enviroment;
import Front.Expression.VariableExpression.FieldExpression;
import Front.Function;
import Front.Type.BasicType.VoidType;
import Front.Type.ClassType.ClassType;
import Front.Type.Type;

import exception.CompileError;
import exception.InternalError;
import Utility.Util;
import java.util.*;

public class FunctionCallExpression extends Expression {
    public Function function;
    public List<Expression> parameters;

    public FunctionCallExpression(Type type, boolean mark, Function function, List<Expression> parameters) {

        super(type, mark);

        this.function = function;
        this.parameters = parameters;
    }
    public static Expression getExpression(Function function, List<Expression> parameters) {
        return new FunctionCallExpression(function.type, false, function, parameters);
    }
    public static Expression getExpression(Expression expression, List<Expression> parameters) {
        if (expression instanceof NewExpression) {
            NewExpression tmp = (NewExpression)expression;
            if (tmp.type instanceof ClassType) {
                ClassType y = (ClassType)tmp.type;
                if (parameters.size() > 0)
                    throw new CompileError("There should not have parameters in constructor.");
                List<Type> x = new ArrayList<Type>();
                x.add(y);
                if (!y.constructors.containsKey(x))
                    throw new CompileError("Class " + y.name + "don't have this constructor");
                tmp.constructor = y.constructors.get(x);
                return tmp;
            }
            throw new CompileError("the " + tmp.type.toString() + "should not have constructor.");
        }
        if (expression.type instanceof Function) {
            Function function = (Function)expression.type;
            if (expression instanceof FieldExpression) parameters.add(0, ((FieldExpression)expression).expression);
            if (parameters.size() != function.parameters.size()) {
                throw new CompileError("the number of parameters is not fitted");
            }
            for (int i = 0; i < parameters.size(); i ++) {
                if (i == 0 && expression instanceof FieldExpression) continue;
                if (function.parameters.get(i).type.comparable(parameters.get(i).type) == false)
                    throw new CompileError("the type of parameters is not fitted.");
            }
            return new FunctionCallExpression(function.type, false, function, parameters);
        }
        throw new CompileError("The type is not Function when you do Function-call.");
    }
    @Override
    public String toString() {
        return "Expression : Function Call: " + function.name;
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(this).append("\n");
        parameters.forEach(parameter -> str.append(parameter.toString(indents + 1)));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instructions) {
        List<Operand> x = new ArrayList<>();
        for (Expression expression : parameters) {
            expression.combine(instructions);
            expression.load(instructions);
            x.add(expression.operand);
        }
        if (type instanceof VoidType)
            instructions.add(CallInstruction.getInstruction(null, function, x));
        else {
            operand = Enviroment.registertable.addTempRegister();
            instructions.add(CallInstruction.getInstruction(operand, function, x));
        }
    }
}