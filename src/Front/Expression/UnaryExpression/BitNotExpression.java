package Front.Expression.UnaryExpression;

import ControlFlowGraph.Instruction.UnaryInstruction.BitNotInstruction;
import Front.Expression.ConstantExpression.IntConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;

import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class BitNotExpression extends UnaryExpression {
	private BitNotExpression(Type type, boolean mark, Expression expression) {
		super(type, mark, expression);
	}

	public static Expression getExpression(Expression expression) {
		if (expression.type instanceof IntType) {
			if (expression instanceof IntConstant) return IntConstant.getConstant(~((IntConstant)expression).value);
			return new BitNotExpression(IntType.getType(), false, expression);
		}
		throw new CompileError("There should be a int type in the bit_not.");
	}

	@Override
	public String toString() {
		return "Expression: bit_not";
	}

	@Override
	public void combine(List<Instruction> instr) {
		expression.combine(instr);
		expression.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(BitNotInstruction.getInstruction(operand, expression.operand));
	}
}
