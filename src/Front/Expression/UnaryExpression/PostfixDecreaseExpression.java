package Front.Expression.UnaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.AdditionInstruction;
import ControlFlowGraph.Instruction.BinaryInstruction.MinusInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import ControlFlowGraph.Operand.Address;
import ControlFlowGraph.Operand.ImmediateValue;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class PostfixDecreaseExpression extends UnaryExpression {
	public PostfixDecreaseExpression(Type type, boolean mark, Expression expression) {
		super(type, mark, expression);
	}

	public static Expression getExpression(Expression expression) {
		if (expression.mark == false) throw new CompileError("You need a left-value in PostfixDecrease.");
		if (expression.type instanceof IntType)	return new PostfixDecreaseExpression(IntType.getType(), false, expression);
		throw new CompileError("You need a int type in PostfixDecrease.");
	}

	@Override
	public String toString() {
		return "Expression: Postfix Decrease";
	}
	@Override
	public void combine(List<Instruction> instructions) {
		expression.combine(instructions);
		operand = Enviroment.registertable.addTempRegister();
		if (expression.operand instanceof Address) {
			Address address = (Address)expression.operand;
			address = new Address(address.base, address.offset, address.size);
			expression.load(instructions);
			instructions.add(MovInstruction.getInstruction(operand, expression.operand));
			instructions.add(MinusInstruction.getInstruction(expression.operand, expression.operand, new ImmediateValue(1)));
			instructions.add(StoreInstruction.getInstruction(expression.operand, address));
		} else {
			expression.load(instructions);
			instructions.add(MovInstruction.getInstruction(operand, expression.operand));
			instructions.add(MinusInstruction.getInstruction(expression.operand, expression.operand, new ImmediateValue(1)));
		}
	}
}
