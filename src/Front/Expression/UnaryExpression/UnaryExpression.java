package Front.Expression.UnaryExpression;

import Front.Expression.Expression;
import Front.Type.Type;
import Utility.Util;

public abstract class UnaryExpression extends Expression {
	public Expression expression;
	public UnaryExpression(Type type, boolean mark, Expression expression) {
		super(type, mark);
		this.expression = expression;
	}
	@Override
	public String toString(int indents) {
		StringBuilder str = new StringBuilder();
		str.append(Util.getindent(indents)).append(toString()).append("\n");
		str.append(expression.toString(indents + 1));
		return str.toString();
	}
}
