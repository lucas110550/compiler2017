package Front.Expression.UnaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.MinusInstruction;
import ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import ControlFlowGraph.Operand.Address;
import ControlFlowGraph.Operand.ImmediateValue;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class PrefixDecreaseExpression extends UnaryExpression {
	public PrefixDecreaseExpression(Type type, boolean mark, Expression expression) {
		super(type, mark, expression);
	}

	public static Expression getExpression(Expression expression) {
		if (expression.mark == false) throw new CompileError("You need a left-value in PrefixDecrease.");
		if (expression.type instanceof IntType)	return new PrefixDecreaseExpression(IntType.getType(), false, expression);
		throw new CompileError("You need a int type in PrefixDecrease.");
	}

	@Override
	public String toString() {
		return "Expression: Prefix Decrease";
	}
	@Override
	public void combine(List<Instruction> instructions) {
		expression.combine(instructions);
		if (expression.operand instanceof Address) {
			Address address = (Address)expression.operand;
			address = new Address(address.base, address.offset, address.size);
			expression.load(instructions);
			operand = expression.operand;
			instructions.add(MinusInstruction.getInstruction(operand, operand, new ImmediateValue(1)));
			instructions.add(StoreInstruction.getInstruction(operand, address));
		} else {
			expression.load(instructions);
			operand = expression.operand;
			instructions.add(MinusInstruction.getInstruction(operand, operand, new ImmediateValue(1)));
		}
	}
}
