package Front.Expression.UnaryExpression;

import ControlFlowGraph.Instruction.BinaryInstruction.BitXorInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import Front.Expression.ConstantExpression.BoolConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.BoolType;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class NotExpression extends UnaryExpression {
	private NotExpression(Type type, boolean mark, Expression expression) {
		super(type, mark, expression);
	}

	public static Expression getExpression(Expression expression) {
		if (expression.type instanceof BoolType) {
			if (expression instanceof BoolConstant) return BoolConstant.getConstant(!((BoolConstant)expression).value);
			return new NotExpression(BoolType.getType(), false, expression);
		}
		throw new CompileError("There should be a bool type in the Not expression.");
	}

	@Override
	public String toString() {
		return "Expression: Not";
	}
	@Override
	public void combine(List<Instruction> instr) {
		expression.combine(instr);
		expression.load(instr);
		operand = Enviroment.registertable.addTempRegister();
		instr.add(BitXorInstruction.getInstruction(operand, expression.operand, new ImmediateValue(1)));
	}
}
