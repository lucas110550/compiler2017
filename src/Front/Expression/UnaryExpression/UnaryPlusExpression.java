package Front.Expression.UnaryExpression;

import Front.Expression.ConstantExpression.IntConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public class UnaryPlusExpression extends UnaryExpression {
	private UnaryPlusExpression(Type type, boolean mark, Expression expression) {
		super(type, mark, expression);
	}

	public static Expression getExpression(Expression expression) {
		if (expression.type instanceof IntType) {
			if (expression instanceof IntConstant) return IntConstant.getConstant(((IntConstant)expression).value);
			return new UnaryPlusExpression(IntType.getType(), false, expression);
		}
		throw new CompileError("There should be a int type in the unary plus.");
	}

	@Override
	public String toString() {
		return "Expression: Unary Plus";
	}
	@Override
	public void combine(List<Instruction> instr) {
		operand = expression.operand;
	}
}
