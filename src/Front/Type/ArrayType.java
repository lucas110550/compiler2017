package Front.Type;

import Front.Expression.ConstantExpression.Constant;
import Front.Expression.ConstantExpression.IntConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.NullType;
import Front.Type.BasicType.VoidType;

import exception.CompileError;
import exception.InternalError;
import java.util.*;

public class ArrayType extends Type {
    public Type baseType;
    public int dimension;
    public List<Expression> expressions;

    public ArrayType(Type baseType, int dimension, List<Expression> expressions) {
        this.baseType = baseType;
        this.dimension = dimension;
        this.expressions = expressions;
    }
    public static Type getType(Type baseType, int dimension, List<Expression> expressions) {
        if (baseType instanceof VoidType) {
            throw new CompileError("BaseType can not be void.");
        }
        if (dimension <= 0) {
            throw new InternalError();
        }
        return new ArrayType(baseType, dimension, expressions);
    }
    public static Type getType(Type baseType) {
        if (baseType instanceof VoidType) {
            throw new CompileError("BaseType can not bne void.");
        }
        if (baseType instanceof ArrayType) {
            ArrayType arr = (ArrayType)(baseType);
            arr.expressions.add(null);
            return new ArrayType(arr.baseType, arr.dimension + 1, arr.expressions);
        }
        else return new ArrayType(baseType, 1, new ArrayList<Expression>());
    }

    public Type reduce() {
        if (dimension == 1) return baseType;
        else {
            List<Expression> x = new ArrayList<Expression>();
            for (int i = 0; i + 1 < expressions.size(); i ++) x.add(expressions.get(i));
            return ArrayType.getType(baseType, dimension - 1, x);
        }
    }

    @Override
    public boolean comparable(Type other) {
        if (other instanceof NullType) return true;
        if (other instanceof ArrayType) {
            ArrayType arr = (ArrayType)(other);
            return arr.baseType.comparable(baseType) && arr.dimension == dimension;
        }
        return false;
    }

    @Override
    public String toString() {
        return "array : " + baseType + "(" + dimension + ")";
    }
}