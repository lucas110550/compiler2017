package Front.Type.BasicType;

import Front.Type.Type;

public class BoolType extends Type {
    static BoolType cur = new BoolType();

    public static Type getType() { return cur; }

    @Override
    public int size() { return 8; }
    @Override
    public boolean comparable(Type other) {
        return other instanceof BoolType;
    }
    @Override
    public String toString() {
        return "bool";
    }
}