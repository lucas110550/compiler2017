package Front.Type.BasicType;

import Front.Type.Type;

public class IntType extends Type {
    static IntType cur = new IntType();

    public static Type getType() { return cur; }

    @Override
    public boolean comparable(Type other) {
        return other instanceof IntType;
    }
    @Override
    public String toString() {
        return "int";
    }
}