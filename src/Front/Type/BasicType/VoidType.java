package Front.Type.BasicType;

import Front.Type.Type;

public class VoidType extends Type {
    static VoidType cur = new VoidType();

    public static Type getType() { return cur; }

    @Override
    public boolean comparable(Type other) {
        return false;
    }
    @Override
    public String toString() {
        return "void";
    }
}