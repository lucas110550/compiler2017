package Front.Type.BasicType;

import Front.Type.Type;

public class StringType extends Type {
    static StringType cur = new StringType();

    public static Type getType() { return cur; }

    @Override
    public boolean comparable(Type other) {
        return other instanceof StringType;
    }
    @Override
    public String toString() {
        return "string";
    }
}