package Front.Type.BasicType;

import Front.Type.ArrayType;
import Front.Type.Type;

public class NullType extends Type {
    static NullType cur = new NullType();

    public static Type getType() { return cur; }

    @Override
    public boolean comparable(Type other) {
        return other instanceof NullType || other instanceof StringType || other instanceof ArrayType;
    }
    @Override
    public String toString() {
        return "null";
    }
}