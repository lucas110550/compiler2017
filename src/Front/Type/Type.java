package Front.Type;

import Front.Node;
import Utility.Util;

public abstract class Type implements Node {
    public int size() { return 8; }
    public abstract boolean comparable(Type other);

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        return str.toString();
    }
}