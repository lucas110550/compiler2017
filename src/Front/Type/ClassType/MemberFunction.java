package Front.Type.ClassType;

import Front.Function;
import Front.Type.ClassType.ClassType;

public class MemberFunction extends Member {
    public Function function;

    public MemberFunction(ClassType Type, String name, Function function) {
        super(name);
        this.function = function;
    }
}