package Front.Type.ClassType;

import Enviroment.Enviroment;
import Scope.Scope;
import Front.Function;
import Front.Type.BasicType.NullType;
import Front.Type.ClassType.Member;
import Front.Type.ClassType.MemberFunction;
import Front.Type.ClassType.MemberVariable;
import Front.Type.Type;

import exception.CompileError;
import Utility.Util;

import java.util.*;

public class ClassType extends Type implements Scope {
    public String name;
    public Map<String, MemberVariable> memberVariables;
    public Map<String, MemberFunction> memberFunctions;
    public Map<List<Type>, Function> constructors;
    public int Size;
    public ClassType(String name) {
        this.name = name;
        this.memberFunctions = new HashMap<>();
        this.memberVariables = new HashMap<>();
        this.constructors = new HashMap<>();
        this.Size = 0;
    }
    public static Type getType(String name) {
        return new ClassType(name);
    }
    public void addMember(String name, Type type) {
        if (name.equals("this"))
            throw new CompileError("This is a reserved word.");
        if (contains(name)) throw new CompileError("This class have two members named same.");
        if (type instanceof Function) {
            Function function = (Function )(type);
            function.name = this.name + "." + function.name;
            MemberFunction tmp = new MemberFunction(this, name, function);
            memberFunctions.put(name, tmp);
        } else {
            MemberVariable tmp = new MemberVariable(this, name, type);
            memberVariables.put(name, tmp);
        }
    }
    public void addConstructor(Function function) {
        List<Type> tmp = function.getTypeList();
        if (constructors.containsKey(tmp)) {
            throw new CompileError("You defined the constructor with same parameters.");
        }
        constructors.put(tmp, function);
        function.name = name + ".construct" + constructors.size();
    }
    public Member getMember(String name) {
        if (memberVariables.containsKey(name)) {
            return memberVariables.get(name);
        }
        if (memberFunctions.containsKey(name)) {
            return memberFunctions.get(name);
        }
        throw new CompileError("No such Member in this Class.");
    }
    public boolean contains(String name) {
        if (memberVariables.containsKey(name)) return true;
        if (memberFunctions.containsKey(name)) return true;
        return false;
    }
    @Override
    public boolean comparable(Type other) {
        return other instanceof NullType || other == this;
    }
    @Override
    public String toString() { return name; }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append("Class: ").append(toString()).append("\n");
        memberVariables.forEach((name, member) -> str.append(member.toString(indents + 1)));
        return str.toString();
    }
}
