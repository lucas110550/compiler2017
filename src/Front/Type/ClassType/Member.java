package Front.Type.ClassType;

import Enviroment.Enviroment;
import Front.Type.ClassType.ClassType;

public abstract class Member {
    public String name;
    public ClassType origin;
    Member(String name) {
        this.name = name;
        this.origin = Enviroment.scopetable.getClassScope();
    }
}