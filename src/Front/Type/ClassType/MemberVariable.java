package Front.Type.ClassType;

import Front.Type.ClassType.ClassType;
import Front.Type.Type;
import Front.Expression.Expression;
import Utility.Util;

public class MemberVariable extends Member {
    public Type type;
    public int offset;
    public Expression expression;
    public MemberVariable(ClassType classType, String name, Type type) {
        super(name);
        this.type = type;
        this.offset = classType.Size;
        classType.Size += Util.getsize(type.size());
    }

    @Override
    public String toString() {
        return "Varible name = " + name + "Type = " + type;
    }

    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        if (expression != null)
            str.append(expression.toString(indents + 1));
        return str.toString();
    }
}