package Front.Statement;

import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.ReturnInstruction;
import Enviroment.Enviroment;
import Front.Expression.Expression;
import Front.Function;
import Front.Type.BasicType.VoidType;

import exception.CompileError;
import Utility.Util;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class ReturnStatement extends Statement {
    public Expression expression;
    public Function to;
    public ReturnStatement(Expression expression, Function to) {
        this.expression = expression;
        this.to = to;
    }
    public static Statement getStatement(Expression expression) {
        Function tmp = Enviroment.scopetable.getFunctionScope();
        if (tmp == null) throw new CompileError("Return should in a function.");
        if (expression == null) {
            if (tmp.type instanceof VoidType)
                return new ReturnStatement(expression, tmp);
        }
        else {
            if (tmp.type.comparable(expression.type))
                return new ReturnStatement(expression, tmp);
        }
        throw new CompileError("Return value does not fit the Function's require");
    }
    @Override
    public String toString() {
        return "Statement: Return";
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        if (expression != null) str.append(expression.toString(indents + 1));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        if (expression != null) {
            expression.combine(instr);
            expression.load(instr);
            instr.add(ReturnInstruction.getInstruction(expression.operand));
        }
        instr.add(JumpInstruction.getInstruction(to.out));
    }
}