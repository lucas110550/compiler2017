package Front.Statement;

import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import Front.Expression.ConstantExpression.BoolConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.BoolType;

import exception.CompileError;
import Utility.Util;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class ForStatement extends LoopStatement {
    public Expression exp1, exp2, exp3;
    public Statement statement;

    public static Statement getStatement() {
        return new ForStatement();
    }

    public void Initialization(Expression exp1) {
        this.exp1 = exp1;
    }
    public void addCondition(Expression exp2) {
        if (exp2 == null) {
            this.exp2 = BoolConstant.getConstant(true);
        } else {
            if (exp2.type instanceof BoolType)
                this.exp2 = exp2;
            else {
                throw new CompileError("Condition should be BoolType.");
            }
        }
    }
    public void addProcess(Expression exp3) {
        this.exp3 = exp3;
    }
    public void addStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public String toString() {
        return "ForStatement: ";
    }

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        if (exp1 != null) str.append(exp1.toString(indents + 1));
        if (exp2 != null) str.append(exp2.toString(indents + 1));
        if (exp3 != null) str.append(exp3.toString(indents + 1));
        str.append(statement.toString(indents + 1));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        LabelInstruction Cond = LabelInstruction.getInstruction("for_Cond");
        LabelInstruction subStatement = LabelInstruction.getInstruction("for_subStatement");
        loop = LabelInstruction.getInstruction("for_loop");
        all = LabelInstruction.getInstruction("for_all");
        if (exp1 != null) exp1.combine(instr);
        instr.add(JumpInstruction.getInstruction(Cond));
        instr.add(Cond);
        if (exp2 == null) addCondition(null);
        exp2.combine(instr);
        instr.add(BranchInstruction.getInstruction(exp2.operand, subStatement, all));
        instr.add(subStatement);
        if (statement != null) statement.combine(instr);
        instr.add(JumpInstruction.getInstruction(loop));
        instr.add(loop);
        if (exp3 != null) exp3.combine(instr);
        instr.add(JumpInstruction.getInstruction(Cond));
        instr.add(all);
    }
}