package Front.Statement;

import Front.Node;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
import Enviroment.Enviroment;
public abstract class Statement implements Node {
    public abstract void combine(List<Instruction> instr);
}