package Front.Statement;

import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import Front.Expression.ConstantExpression.BoolConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.BoolType;
import Scope.Scope;
import exception.CompileError;
import Utility.Util;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class IfStatement extends Statement implements Scope {
    public Expression condition;
    public Statement Statement1, Statement2;

    public IfStatement(Expression condition, Statement Statement1, Statement Statement2) {
        this.condition = condition;
        this.Statement1 = Statement1;
        this.Statement2 = Statement2;
    }
    public static Statement getStatement(Expression condition, Statement Statement1, Statement Statement2) {
        if (condition.type instanceof BoolType) {
            if (condition instanceof BoolConstant) {
                if (((BoolConstant) condition).value)
                    return Statement1;
                else return Statement2;
            }
            return new IfStatement(condition, Statement1, Statement2);
        }
        else throw new CompileError("If condition should be a bool type.");
    }
    @Override
    public String toString() {
        return "Statement: if";
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        str.append(condition.toString(indents + 1));
        if (Statement1 != null)
            str.append(Statement1.toString(indents + 1));
        if (Statement2 != null)
            str.append(Statement2.toString(indents + 1));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        LabelInstruction trueCond = LabelInstruction.getInstruction("trueCond");
        LabelInstruction falseCond = LabelInstruction.getInstruction("falseCond");
        LabelInstruction allCond = LabelInstruction.getInstruction("allCond");
        condition.combine(instr);
        condition.load(instr);
        instr.add(BranchInstruction.getInstruction(condition.operand, trueCond, falseCond));
        instr.add(trueCond);
        if (Statement1 != null)
            Statement1.combine(instr);
        instr.add(JumpInstruction.getInstruction(allCond));
        instr.add(falseCond);
        if (Statement2 != null)
            Statement2.combine(instr);
        instr.add(JumpInstruction.getInstruction(allCond));
        instr.add(allCond);
    }
}