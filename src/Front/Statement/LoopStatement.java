package Front.Statement;

import ControlFlowGraph.Instruction.LabelInstruction;
import Scope.Scope;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public abstract class LoopStatement extends Statement implements Scope {
    public LabelInstruction loop, all;
}