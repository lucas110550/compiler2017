package Front.Statement;

import Front.Expression.Expression;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class ExpressionStatement extends Statement {
    public Expression expression;
    public ExpressionStatement(Expression expression) {
        this.expression = expression;
    }
    public static Statement getStatement(Expression expression) {
        return new ExpressionStatement(expression);
    }
    @Override
    public String toString() {
        return "Statement: Expression";
    }
    @Override
    public String toString(int indents) {
        if (expression == null) return null;
        return expression.toString(indents);
    }
    @Override
    public void combine(List<Instruction> instr) {
        if (expression != null) expression.combine(instr);
    }
}