package Front.Statement;

import Scope.Scope;
import Utility.Util;
import java.util.ArrayList;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class CompondStatement extends Statement implements Scope {
    public ArrayList<Statement> statements;

    public CompondStatement() {
        statements = new ArrayList<>();
    }
    public static Statement getStatement() {
        return new CompondStatement();
    }
    public void add(Statement statement) {
        statements.add(statement);
    }

    @Override
    public String toString() {
        return "CompondStatement:";
    }

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        for (Statement x : statements) {
            str.append(x.toString(indents + 1));
        }
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        statements.forEach(statement -> statement.combine(instr));
    }
}