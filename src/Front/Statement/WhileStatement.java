package Front.Statement;

import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import Front.Expression.ConstantExpression.BoolConstant;
import Front.Expression.Expression;
import Front.Type.BasicType.BoolType;

import exception.CompileError;
import Utility.Util;

import java.awt.*;
import java.util.*;
import java.util.List;

import ControlFlowGraph.Instruction.Instruction;
public class WhileStatement extends LoopStatement {
    public Expression condition;
    public Statement statement;

    public static Statement getStatement() {
        return new WhileStatement();
    }

    public void addCondition(Expression condition) {
        if (condition.type instanceof BoolType)
            this.condition = condition;
        else {
            throw new CompileError("Condition should be BoolType.");
        }
    }
    public void addStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public String toString() {
        return "WhileStatement: ";
    }

    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        str.append(condition.toString(indents + 1));
        str.append(statement.toString(indents + 1));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        LabelInstruction subStatement = LabelInstruction.getInstruction("while_sub");
        loop = LabelInstruction.getInstruction("while_loop");
        all = LabelInstruction.getInstruction("while_all");
        instr.add(JumpInstruction.getInstruction(loop));
        instr.add(loop);
        condition.combine(instr);
        instr.add(BranchInstruction.getInstruction(condition.operand, subStatement, all));
        instr.add(subStatement);
        statement.combine(instr);
        instr.add(JumpInstruction.getInstruction(loop));
        instr.add(all);
    }
}