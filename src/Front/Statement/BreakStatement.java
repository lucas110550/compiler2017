package Front.Statement;

import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import Enviroment.Enviroment;
import exception.CompileError;
import Utility.Util;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class BreakStatement extends Statement {
    public LoopStatement to;
    public BreakStatement(LoopStatement to) {
        this.to = to;
    }
    public static Statement getStatement() {
        if (Enviroment.scopetable.getLoopScope() == null) {
            throw new CompileError("There is not any Scope to break.");
        }
        return new BreakStatement(Enviroment.scopetable.getLoopScope());
    }
    @Override
    public String toString() {
        return "Statement: Break";
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents + 1)).append(toString()).append("\n");
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        instr.add(JumpInstruction.getInstruction(to.all));
    }
}