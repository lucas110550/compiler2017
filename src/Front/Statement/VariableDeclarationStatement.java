package Front.Statement;

import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import Symbol.Symbol;
import Front.Expression.Expression;
import Front.Type.BasicType.VoidType;
import exception.CompileError;
import Utility.Util;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class VariableDeclarationStatement extends Statement {
    public Symbol symbol;
    public Expression expression;

    public VariableDeclarationStatement(Symbol symbol, Expression expression) {
        this.symbol = symbol;
        this.expression = expression;
    }
    public static Statement getStatement(Symbol symbol, Expression expression) {
        if (symbol.name.equals("this")) throw new CompileError("This is a reserved word.");
        if (symbol.type instanceof VoidType)
            throw new CompileError("Can't get value to a void type.");
        if (expression == null || symbol.type.comparable(expression.type))
            return new VariableDeclarationStatement(symbol, expression);
        throw new CompileError("Declared Indentifier's type does not match the expression.");
    }
    @Override
    public String toString() {
        return "Statement: VariableDeclaration name = " + symbol.name + " type = " + symbol.type;
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(this).append("\n");
        if (expression != null)
            str.append(expression.toString(indents + 1));
        return str.toString();
    }
    @Override
    public void combine(List<Instruction> instr) {
        if (expression != null) {
            expression.combine(instr);
            expression.load(instr);
            instr.add(MovInstruction.getInstruction(symbol.register, expression.operand));
        }
    }
}