package Front;

import Front.Statement.VariableDeclarationStatement;
import Front.Type.ClassType.ClassType;
import Utility.Util;
import Scope.Scope;
import com.sun.org.apache.xpath.internal.operations.Variable;

import java.util.*;

public class Program implements Node, Scope {
    public List<Function> functions;
    public List<VariableDeclarationStatement> variables;
    public List<ClassType> classtypes;

    public Program() {
        functions = new ArrayList<>();
        variables = new ArrayList<>();
        classtypes = new ArrayList<>();
    }
    public static Program getProgram() { return new Program(); }
    public void addClassType(ClassType other) {
        classtypes.add(other);
    }
    public void addFunction(Function other) {
        functions.add(other);
    }
    public void addVariable(VariableDeclarationStatement other) {
        variables.add(other);
    }
    @Override
    public String toString() {
        return "Program";
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        functions.forEach(function -> {
            str.append(function.toString(indents + 1));
        });
        classtypes.forEach(classType -> {
            str.append(classType.toString(indents + 1));
        });
        variables.forEach(variable -> {
            str.append(variable.toString(indents + 1));
        });
        return str.toString();
    }

}