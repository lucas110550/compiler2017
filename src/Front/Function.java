package Front;

import Allocate.Allocate;
import ControlFlowGraph.Graph;
import ControlFlowGraph.Instruction.LabelInstruction;
import Enviroment.Enviroment;
import Scope.Scope;
import Symbol.Symbol;
import Front.Statement.CompondStatement;

import Front.Type.BasicType.IntType;
import Front.Type.Type;
import exception.CompileError;
import Utility.Util;

import java.util.*;
import java.util.*;
import ControlFlowGraph.Instruction.Instruction;
public class Function extends Type implements Node, Scope {
    public String name;
    public Type type;
    public List<Symbol> parameters;
    public CompondStatement statements;
    public LabelInstruction enter, subStatement, out;
    public Graph graph;
    public Allocate allocate;
    public Function(String name, Type type, List<Symbol> parameters) {
        this.name = name;
        this.type = type;
        this.parameters = parameters;
    }
    public static Function getFunction(String name, Type returnvalue, List<Symbol> parameters) {
        if (name.equals("main")) {
            if (returnvalue instanceof IntType) {
                if (parameters.size() != 0)
                    throw new CompileError("Main function should have no parameter.");
            } else throw new CompileError("Returnvalue should be int type.");
        }
        if (Enviroment.scopetable.getClassScope() == null) {
            if (Enviroment.symboltable.contains(name) == true)
                throw new CompileError("There are two function named same in this Scope.");
        }
        boolean ok = false;
        for (int i = 0; i < (int) (parameters.size()); i++)
            for (int j = i + 1; j < (int) (parameters.size()); j++)
                if (parameters.get(i).name.equals(parameters.get(j).name)) {
                    ok = true;
                    break;
                }
        if (ok == true) throw new CompileError("This Function have two parameters named same.");
        return new Function(name, returnvalue, parameters);
    }

    public List<Type> getTypeList() {
        List<Type> paraType = new ArrayList<>();
        parameters.forEach(x -> paraType.add(x.type));
        return paraType;
    }
    public void addStatements(CompondStatement statements) {
        this.statements = statements;
    }
    @Override
    public boolean comparable(Type type) {
        return false;
    }
    @Override
    public String toString() {
        return "Function : name = " + name + "Type = " + type;
    }
    @Override
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(toString()).append("\n");
        parameters.forEach(x -> {
            str.append(Util.getindent(indents + 1));
            str.append("parameters : name = ").append(x.name).append("Type = ").append(x.type).append("\n");
        });
        if (statements != null) str.append(statements.toString(indents + 1));
        return str.toString();
    }
}