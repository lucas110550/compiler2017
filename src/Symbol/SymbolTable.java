package Symbol;

import Front.Type.Type;
import exception.CompileError;
import Enviroment.Enviroment;

import java.util.*;

public class SymbolTable {
    public Map<String, Stack<Symbol>> allsymbol;
    public Stack<Map<String, Symbol>> symboltable;

    public SymbolTable() {
        allsymbol = new HashMap<>();
        symboltable = new Stack<>();
    }
    public Symbol add(String str, Type type) {
        if (symboltable.peek().containsKey(str))
            throw new CompileError("There's symbol named " + str + "\n");
        if (!allsymbol.containsKey(str)) allsymbol.put(str, new Stack<>());
        Symbol tmp = new Symbol(str, type);
        allsymbol.get(str).push(tmp);
        symboltable.peek().put(str, tmp);
        return tmp;
    }
    public Symbol addGlobalVariable(String name, Type type){
        Symbol symbol = add(name, type);
        symbol.register = Enviroment.registertable.addGlobalRegister(symbol);
        return symbol;
    }
    public Symbol addParameterVariable(String name, Type type){
        Symbol symbol = add(name, type);
        symbol.register = Enviroment.registertable.addParameterRegister(symbol);
        return symbol;
    }
    public Symbol addTempVariable(String name, Type type){
        Symbol symbol = add(name, type);
        symbol.register = Enviroment.registertable.addTempRegister(symbol);
        return symbol;
    }
    public Symbol find(String str) {
        return allsymbol.get(str).peek();
    }
    public void intoScope() {
        symboltable.push(new HashMap<>());
    }
    public void outScope() {
        symboltable.pop().forEach((name, symbol) -> allsymbol.get(name).pop());
    }
    public boolean contains(String str) {
        if (!allsymbol.containsKey(str)) return false;
        if (allsymbol.get(str).empty()) return false;
        return true;
    }
}