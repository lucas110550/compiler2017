package Symbol;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Type.Type;
import Scope.Scope;
import Enviroment.Enviroment;
public class Symbol {
    public String name;
    public Type type;
    public Scope scope;
    public VirtualRegister register;
    public Symbol(String name, Type type) {
        this.name = name;
        this.type = type;
        this.scope = Enviroment.scopetable.getScope();
        this.register = null;
    }
}