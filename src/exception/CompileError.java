package exception;

import CST.Listener.BaseListener;

public class CompileError extends Error {
    public CompileError(String errorMessage) {
        super("Compilation error: R: " + BaseListener.r + ", C: " + BaseListener.c + ": " + errorMessage + "!");
    }
}