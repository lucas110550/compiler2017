package Scope;

import Front.Function;
import Front.Statement.LoopStatement;
import Front.Type.ClassType.ClassType;

import java.util.*;

public class ScopeTable {
    public Stack<Scope> allscope;
    public Stack<Function> FunctionScope;
    public Stack<LoopStatement> LoopScope;
    public Stack<ClassType> ClassScope;

    public ScopeTable() {
        allscope = new Stack<>();
        FunctionScope = new Stack<>();
        LoopScope = new Stack<>();
        ClassScope = new Stack<>();
    }
    public void intoScope(Scope other) {
        allscope.push(other);
        if (other instanceof Function)
            FunctionScope.push((Function)other);
        if (other instanceof LoopStatement)
            LoopScope.push((LoopStatement)other);
        if (other instanceof ClassType)
            ClassScope.push((ClassType)other);
    }
    public void outScope() {
        if (allscope.empty()) throw new InternalError();
        Scope tmp = allscope.peek();
        allscope.pop();
        if (tmp instanceof Function) {
            if (FunctionScope.empty()) throw new InternalError();
            FunctionScope.pop();
        }
        if (tmp instanceof LoopStatement) {
            if (LoopScope.empty()) throw new InternalError();
            LoopScope.pop();
        }
        if (tmp instanceof ClassType) {
            if (ClassScope.empty()) throw new InternalError();
            ClassScope.pop();
        }
    }
    public Scope getScope() {
        if (allscope.empty()) return null;
        return allscope.peek();
    }
    public Function getFunctionScope() {
        if (FunctionScope.empty()) return null;
        return FunctionScope.peek();
    }
    public LoopStatement getLoopScope() {
        if (LoopScope.empty()) return null;
        return LoopScope.peek();
    }
    public ClassType getClassScope() {
        if (ClassScope.empty()) return null;
        return ClassScope.peek();
    }
}