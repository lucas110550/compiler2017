package Utility;

import java.util.*;

public class Util {
    public static String getindent(int indents) {
        StringBuilder str = new StringBuilder();
        for (int i = 1; i <= indents; i ++)
            str.append("\t");
        return str.toString();
    }
    public static int getsize(int size) {
        return (size + 7) / 8 * 8;
    }
}