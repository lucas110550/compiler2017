import Allocate.GlobalRegisterAllocator.GlobalRegisterAllocator;
import CST.Parser.MargatroidParser;
import ControlFlowGraph.Graph;
import Enviroment.Enviroment;
import Front.Function;
import CST.Listener.*;
import CST.Parser.MargatroidLexer;
import CST.Parser.MargatroidListener;

import Front.Program;
import Translator.NASM.NASMTranslator.MtoMTranslator;
import Translator.NASM.NASMTranslator.RtoRTranslator;
import Translator.NASM.NASMTranslator.NASMTranslator;
import exception.CompileError;
import exception.InternalError;
import Utility.Util;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.*;
import java.util.*;

    public class Main {
        public static void main(String[] args) throws Exception {
            InputStream is = new FileInputStream("/home/lucas110550/compiler2017/src/test.txt");
            InputStreamReader Src = new InputStreamReader(is);
            ANTLRInputStream input = new ANTLRInputStream(Src);
            MargatroidLexer lexer = new MargatroidLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MargatroidParser parser = new MargatroidParser(tokens);
            Enviroment.initialize();
            ParseTree tree = parser.program();
            if (parser.getNumberOfSyntaxErrors() > 0) {
                throw new CompileError("Syntax Error");
            }
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(new FindClassListener(), tree);
            walker.walk(new FindDeclarationListener(), tree);
            walker.walk(new BuildASTListener(), tree);
            if (!Enviroment.symboltable.contains("main"))
                throw new CompileError("You should have main function");
            for (Function function : Enviroment.program.functions) {
                function.graph = new Graph(function);
               function.allocate = new GlobalRegisterAllocator(function);
            }
            new RtoRTranslator(new PrintStream(System.out)).translate();
        }
    }
