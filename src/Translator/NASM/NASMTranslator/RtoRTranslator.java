package Translator.NASM.NASMTranslator;

import Allocate.Allocate;
import Allocate.PhysicalRegister;
import ControlFlowGraph.Block;
import ControlFlowGraph.Graph;
import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Instruction.BinaryInstruction.*;
import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.ControlFlowInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.FunctionInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.ReturnInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.MemoryInstruction.*;
import ControlFlowGraph.Instruction.UnaryInstruction.UnaryInstruction;
import ControlFlowGraph.Operand.Address;
import ControlFlowGraph.Operand.ImmediateValue;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.StringRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TempRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.VariableRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Function;
import Translator.NASM.NASMRegister;
import exception.InternalError;

import java.io.*;

public class RtoRTranslator extends NASMTranslator {
    private Graph graph;
    private Allocate allocator;

    public RtoRTranslator(PrintStream output) {
        super(output);
    }

    private PhysicalRegister loadToRead(Operand from, PhysicalRegister to) {
        if (from instanceof VirtualRegister) {
            if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister)
                    output.printf("\tmov %s, %s\n", to, getGlobalVariableName(from));
                else if (from instanceof ParameterRegister)
                    output.printf("\tmov %s, qword [%s+%d]\n", to, NASMRegister.rsp, graph.frame.getOffset(from));
                else if (from instanceof TempRegister) {
                    PhysicalRegister register = allocator.mapping.get(from);
                    if (register != null) return register;
                    else output.printf("\tmov %s, qword [%s+%d]\n", to, NASMRegister.rsp, graph.frame.getOffset(from));
                }
                else throw new InternalError();
            }
            else if (from instanceof StringRegister) output.printf("\tmov %s, %s\n", to, StringConstantName(from));
            else throw new InternalError();
        }
        else if (from instanceof ImmediateValue) {
            output.printf("\tmov %s, %s\n", to, from);
        }
        else throw new InternalError();
        return to;
    }

    private PhysicalRegister loadToWrite(VirtualRegister from, PhysicalRegister to) {
        if (from instanceof TempRegister) {
            PhysicalRegister register = allocator.mapping.get(from);
            if (register != null) return register;
        }
        return to;
    }

    private void store(VirtualRegister from, PhysicalRegister to) {
        if (from instanceof VariableRegister) {
            if (from instanceof GlobalRegister)
                output.printf("\tmov %s, %s\n", getGlobalVariableName(from), to);
            else if (from instanceof ParameterRegister)
                output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.getOffset(from), to);
            else if (from instanceof TempRegister) {
                PhysicalRegister register = allocator.mapping.get(from);
                if (register == null)
                    output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.getOffset(from), to);
            }
            else throw new InternalError();
        }
        else throw new InternalError();
    }
    private void Load(Allocate allocate, int ddx) {
        for (PhysicalRegister register : allocate.getUsedPhysicalRegister()) {
            if (register == NASMRegister.r8 || register == NASMRegister.r9 || register == NASMRegister.r10 || register == NASMRegister.r11)
                output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, ddx - register.index * 8, register);
        }
    }
    private void Free(Allocate allocate, int ddx) {
        for (PhysicalRegister register : allocate.getUsedPhysicalRegister()) {
            if (register == NASMRegister.r8 || register == NASMRegister.r9 || register == NASMRegister.r10 || register == NASMRegister.r11)
                output.printf("\tmov %s, qword [%s+%d]\n", register, NASMRegister.rsp, ddx - register.index * 8);
        }
    }
    private void move(Operand from, PhysicalRegister to) {
        if (from instanceof VirtualRegister) {
            if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister)
                    output.printf("\tmov %s, %s\n", to, getGlobalVariableName(from));
                else if (from instanceof ParameterRegister)
                    output.printf("\tmov %s, qword [%s+%d]\n", to, NASMRegister.rsp, graph.frame.getOffset(from));
                else if (from instanceof TempRegister) {
                    PhysicalRegister register = allocator.mapping.get(from);
                    if (register != null) {
                        if (register != to) output.printf("\tmov %s, %s\n", to, register);
                    }
                    else output.printf("\tmov %s, qword [%s+%d]\n", to, NASMRegister.rsp, graph.frame.getOffset(from));
                }
                else throw new InternalError();
            }
            else if (from instanceof StringRegister) {
                output.printf("\tmov %s, %s\n", to, StringConstantName(from));
            }
            else throw new InternalError();
        }
        else if (from instanceof Address) throw new InternalError();
        else if (from instanceof ImmediateValue) output.printf("\tmov %s, %s\n", to, from);
        else throw new InternalError();
    }

    private void move(PhysicalRegister from, VirtualRegister to) {
        if (to instanceof VariableRegister) {
            if (to instanceof GlobalRegister) {
                output.printf("\tmov %s, %s\n", getGlobalVariableName(to), from);
            }
            else if (to instanceof ParameterRegister) {
                output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.getOffset(to), from);
            }
            else if (to instanceof TempRegister) {
                PhysicalRegister register = allocator.mapping.get(to);
                if (register != null) {
                    if (register != from) output.printf("\tmov %s, %s\n", register, from);
                }
                else output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.getOffset(to), from);
            }
            else throw new InternalError();
        }
        else throw new InternalError();
    }

    @Override
    public void translate(Graph graph) {
        this.graph = graph;
        this.allocator = graph.function.allocate;
        output.printf("%s:\n", getFunctionName(graph.function));
        output.printf("\tpush %s\n", NASMRegister.rbp);
        output.printf("\tmov %s, %s\n", NASMRegister.rbp, NASMRegister.rsp);
        output.printf("\tsub %s, %d\n", NASMRegister.rsp, graph.frame.size);

        int cnt = graph.frame.size;
        int cc = graph.frame.size;
        if (!graph.function.name.equals("main")) {
            for (PhysicalRegister register : allocator.getUsedPhysicalRegister()) {
                output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.size - register.index * 8, register);
            }
        }
        graph.rebuild();
        for (int k = 0; k < graph.blocks.size(); ++k) {
            Block block = graph.blocks.get(k);
            output.printf("\n%s:\n", getBlockName(block));
            Instruction reserved = null;
            for (int l = 0; l < block.instructions.size(); ++l) {
                Instruction instruction = block.instructions.get(l);
                if (instruction instanceof ArithInstruction) {
                    if (instruction instanceof BinaryInstruction) {
                        BinaryInstruction i = (BinaryInstruction)instruction;
                        if (l + 1 < block.instructions.size() && block.instructions.get(l + 1) instanceof BranchInstruction) {
                            BranchInstruction j = (BranchInstruction)block.instructions.get(l + 1);
                            if (j.cond == i.d && !block.LiveOut.contains(i.d)) {
                                reserved = instruction;
                                continue;
                            }
                        }
                        if (i.r instanceof ImmediateValue) {
                            PhysicalRegister a = loadToRead(i.l, NASMRegister.rax);
                            PhysicalRegister c = loadToWrite(i.d, NASMRegister.r12);
                            if (i.name().equals("div") || i.name().equals("rem")) {
                                if (a != NASMRegister.rax) output.printf("\tmov %s, %s\n", NASMRegister.rax, a);
                                move(i.r, NASMRegister.r13);
                                output.printf("\tcqo\n");
                                output.printf("\tidiv %s\n", NASMRegister.r13);
                                if (i.name().equals("rem")) output.printf("\tmov %s, %s\n", c, NASMRegister.rdx);
                                else output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            else if (i.name().equals("sal") || i.name().equals("sar")) {
                                if (a != NASMRegister.rax) output.printf("\tmov %s, %s\n", NASMRegister.rax, a);
                                output.printf("\t%s %s, %s\n", i.name(), NASMRegister.rax, i.r);
                                output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            else if (i.name().equals("g") || i.name().equals("ge") || i.name().equals("l") || i.name().equals("le") || i.name().equals("e") || i.name().equals("ne")) {
                                output.printf("\tcmp %s, %s\n", a, i.r);
                                output.printf("\tset%s al\n", i.name());
                                output.printf("\tmovzx eax, al\n");
                                output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            else {
                                if (a != NASMRegister.rax) output.printf("\tmov %s, %s\n", NASMRegister.rax, a);
                                output.printf("\t%s %s, %s\n", i.name(), NASMRegister.rax, i.r);
                                output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            store(i.d, c);
                        } else {
                            PhysicalRegister c = loadToWrite(i.d, NASMRegister.r13);
                            if (i.name().equals("div") || i.name().equals("rem")) {
                                PhysicalRegister a = loadToRead(i.l, NASMRegister.rax);
                                PhysicalRegister b = loadToRead(i.r, NASMRegister.r12);
                                if (a != NASMRegister.rax) output.printf("\tmov %s, %s\n", NASMRegister.rax, a);
                                output.printf("\tcqo\n");
                                output.printf("\tidiv %s\n", b);
                                if (i.name().equals("rem")) output.printf("\tmov %s, %s\n", c, NASMRegister.rdx);
                                else output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            else if (i.name().equals("sal") || i.name().equals("sar")) {
                                PhysicalRegister a = loadToRead(i.l, NASMRegister.r12);
                                PhysicalRegister b = loadToRead(i.r, NASMRegister.rax);
                                if (b != NASMRegister.rax) output.printf("\tmov %s, %s\n", NASMRegister.rax, b);
                                output.printf("\tmov edx, eax\n");
                                output.printf("\tmov %s, %s\n", NASMRegister.rax, a);
                                output.printf("\tmov ecx, edx\n");
                                output.printf("\t%s %s, cl\n", i.name(), NASMRegister.rax);
                                output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            else if (i.name().equals("g") || i.name().equals("ge") || i.name().equals("l") || i.name().equals("le") || i.name().equals("e") || i.name().equals("ne")) {
                                PhysicalRegister a = loadToRead(i.l, NASMRegister.rax);
                                PhysicalRegister b = loadToRead(i.r, NASMRegister.r12);
                                output.printf("\tcmp %s, %s\n", a, b);
                                output.printf("\tset%s al\n", i.name());
                                output.printf("\tmovzx eax, al\n");
                                output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                            }
                            else {
                                PhysicalRegister a = loadToRead(i.l, NASMRegister.rax);
                                PhysicalRegister b = loadToRead(i.r, NASMRegister.r12);

                                if (i.name().equals("add")) output.printf("\tlea %s, [%s+%s]\n", c, a, b);
                                else {

                                    if (a != NASMRegister.rax) output.printf("\tmov %s, %s\n", NASMRegister.rax, a);
                                    output.printf("\t%s %s, %s\n", i.name(), NASMRegister.rax, b);
                                    output.printf("\tmov %s, %s\n", c, NASMRegister.rax);
                                }
                            }
                            store(i.d, c);
                        }
                    } else if (instruction instanceof UnaryInstruction) {
                        UnaryInstruction i = (UnaryInstruction)instruction;
                        PhysicalRegister a = loadToRead(i.s, NASMRegister.r12);
                        PhysicalRegister b = loadToWrite(i.d, NASMRegister.r12);
                        if (a != b) output.printf("\tmov %s, %s\n", b, a);
                        output.printf("\t%s %s\n", i.name(), b);
                        store(i.d, b);
                    } else {
                        throw new InternalError();
                    }
                } else if (instruction instanceof ControlFlowInstruction) {
                    if (instruction instanceof BranchInstruction) {
                        BranchInstruction i = (BranchInstruction)instruction;
                        if (reserved == null) {
                            PhysicalRegister a = loadToRead(i.cond, NASMRegister.r12);
                            output.printf("\tcmp %s, 1\n", a);
                            output.printf("\tjnz %s\n", getBlockName(i.FalseCond.block));
                        } else {
                            BinaryInstruction j = (BinaryInstruction)reserved;
                            if (j instanceof BitXorInstruction) {
                                PhysicalRegister a = loadToRead(j.l, NASMRegister.r12);
                                output.printf("\tcmp %s, 0\n", a);
                                output.printf("\tjnz %s\n", getBlockName(i.FalseCond.block));
                            } else {
                                PhysicalRegister a = loadToRead(j.l, NASMRegister.r12);
                                PhysicalRegister b = loadToRead(j.r, NASMRegister.r13);
                                output.printf("\tcmp %s, %s\n", a, b);
                                if (j instanceof EqualToInstruction) {
                                    output.printf("\tjne %s\n", getBlockName(i.FalseCond.block));
                                } else if (j instanceof GreaterThanInstruction) {
                                    output.printf("\tjle %s\n", getBlockName(i.FalseCond.block));
                                } else if (j instanceof GEQInstruction) {
                                    output.printf("\tjl %s\n", getBlockName(i.FalseCond.block));
                                } else if (j instanceof LessThanInstruction) {
                                    output.printf("\tjge %s\n", getBlockName(i.FalseCond.block));
                                } else if (j instanceof LEQInstruction) {
                                    output.printf("\tjg %s\n", getBlockName(i.FalseCond.block));
                                } else if (j instanceof NEQInstruction) {
                                    output.printf("\tje %s\n", getBlockName(i.FalseCond.block));
                                }
                            }
                            reserved = null;
                        }
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.TrueCond.block) {
                            output.printf("\tjmp %s\n", getBlockName(i.TrueCond.block));
                        }
                    } else if (instruction instanceof JumpInstruction) {
                        JumpInstruction i = (JumpInstruction)instruction;
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.to.block) {
                            output.printf("\tjmp %s\n", getBlockName(i.to.block));
                        }
                    } else {
                        throw new InternalError();
                    }
                } else if (instruction instanceof FunctionInstruction) {
                    if (instruction instanceof CallInstruction) {
                        CallInstruction i = (CallInstruction)instruction;
                    Function function = i.function;

                    if (function.name.startsWith("___")) {
                        if (i.parameter.size() >= 1) {
                            move(i.parameter.get(0), NASMRegister.rdi);
                        }
                        if (i.parameter.size() >= 2) {
                            move(i.parameter.get(1), NASMRegister.rsi);
                        }
                        if (i.parameter.size() >= 3) {
                            move(i.parameter.get(2), NASMRegister.rdx);
                        }
                        if (i.parameter.size() >= 4) {
                            move(i.parameter.get(3), NASMRegister.rcx);
                        }
                        if (cnt % 16 != 0) {
                            Load(graph.function.allocate, cc);
                            output.printf("\tsub %s, %d\n", NASMRegister.rsp, 8);
                            output.printf("\tcall %s\n", getFunctionName(function));
                            output.printf("\tadd %s, %d\n", NASMRegister.rsp, 8);
                            Free(graph.function.allocate, cc);
                        }
                        else {
                            Load(graph.function.allocate, cc);
                            output.printf("\tcall %s\n", getFunctionName(function));
                            Free(graph.function.allocate, cc);
                        }
                    } else {
                        boolean mark = false;

                        for (int p = 0; p < function.parameters.size(); ++p) {
                            PhysicalRegister a = loadToRead(i.parameter.get(p), NASMRegister.r12);
                            int offset = function.graph.frame.size - function.graph.frame.getOffset(function.parameters.get(p).register);
                            int xx = 16;
                            if (cnt % 16 != 0) xx += 8;
                            output.printf("\tmov qword [%s-%d], %s\n", NASMRegister.rsp, offset + xx, a);
                        }
                        Load(graph.function.allocate, cc);
                        if (cnt % 16 != 0) {
                            output.printf("\tsub %s, %d\n", NASMRegister.rsp, 8);
                            mark = true;
                        }
                        output.printf("\tcall %s\n", getFunctionName(function));
                        if (mark) output.printf("\tadd %s, %d\n", NASMRegister.rsp, 8);
                        Free(graph.function.allocate, cc);

                    }
                        if (i.d != null) move(NASMRegister.rax, i.d);

                    } else if (instruction instanceof ReturnInstruction) {
                        ReturnInstruction i = (ReturnInstruction)instruction;
                        move(i.s, NASMRegister.rax);
                        if (!graph.function.name.equals("main")) {
                            for (PhysicalRegister register : allocator.getUsedPhysicalRegister()) {
                                output.printf("\tmov %s, qword [%s+%d]\n", register, NASMRegister.rsp, graph.frame.size - register.index * 8);
                            }
                        }
                        output.printf("\tleave\n\tret\n");
                    } else {
                        throw new InternalError();
                    }
                } else if (instruction instanceof MemoryInstruction) {
                    if (instruction instanceof AllocateInstruction) {
                        AllocateInstruction i = (AllocateInstruction)instruction;
                        move(i.size, NASMRegister.rdi);
                        if (cnt % 16 != 0) {
                            Load(graph.function.allocate, cc);
                            output.printf("\tsub %s, %d\n", NASMRegister.rsp, 8);
                            output.printf("\tcall malloc\n");
                            output.printf("\tadd %s, %d\n", NASMRegister.rsp, 8);
                            Free(graph.function.allocate, cc);
                        }
                        else {
                            Load(graph.function.allocate, cc);
                            output.printf("\tcall malloc\n");
                            Free(graph.function.allocate, cc);
                        }
                        move(NASMRegister.rax, i.d);
                    } else if (instruction instanceof LoadInstruction) {
                        LoadInstruction i = (LoadInstruction)instruction;
                        PhysicalRegister a = loadToRead(i.address.base, NASMRegister.rax);
                        PhysicalRegister b = loadToWrite(i.d, NASMRegister.r12);
                        output.printf("\tmov %s, qword [%s+%d]\n", b, a, i.address.offset.value);
                        store(i.d, b);
                    } else if (instruction instanceof MovInstruction) {
                        MovInstruction i = (MovInstruction)instruction;
                        if (i.s instanceof ImmediateValue) {
                            PhysicalRegister a = loadToWrite(i.d, NASMRegister.r12);
                            output.printf("\tmov %s, %s\n", a, i.s);
                            store(i.d, a);
                        } else {
                            PhysicalRegister a = loadToRead(i.s, NASMRegister.r12);
                            move(a, i.d);
                        }
                    } else if (instruction instanceof StoreInstruction) {
                        StoreInstruction i = (StoreInstruction)instruction;
                        PhysicalRegister a = loadToRead(i.address.base, NASMRegister.r12);
                        PhysicalRegister b = loadToRead(i.s, NASMRegister.rax);
                        output.printf("\tmov qword [%s+%d], %s\n", a, i.address.offset.value, b);
                    } else {
                        throw new InternalError();
                    }
                } else {
                    throw new InternalError();
                }
            }
        }
        if (!graph.function.name.equals("main")) {
            for (PhysicalRegister register : allocator.getUsedPhysicalRegister()) {
                output.printf("\tmov %s, qword [%s+%d]\n", register, NASMRegister.rsp, graph.frame.size - register.index * 8);
            }
        }
        output.printf("\tadd %s, %d\n", NASMRegister.rsp, graph.frame.size);
        output.printf("\tpop %s\n", NASMRegister.rbp);
        output.printf("\tret\n");
    }
}
