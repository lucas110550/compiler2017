package Translator.NASM.NASMTranslator;

import ControlFlowGraph.Block;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Enviroment.Enviroment;
import Front.Function;
import Translator.Translator;
import Translator.BuiltinFunction;
import java.io.*;

public class NASMTranslator extends Translator{
    public NASMTranslator(PrintStream output){
        super(output);
    }
    String getFunctionName(Function function) {
        if (function.name.equals("main") || function.name.startsWith("___") || function.name.equals("print_Int")) return function.name;
        else return String.format("____%s___function", function.name);
    }
    String getBlockName(Block block) {
        return String.format("____%s_%d___%s", block.function.name, block.index, block.name);
    }
    String getGlobalVariableName(Operand operand) {
        return String.format("qword [____global_variable_%d]", ((VirtualRegister)operand).index);
    }
    String StringConstantName(Operand operand) {
        return String.format("___global_string_%d", ((VirtualRegister)operand).index);
    }
    @Override
    public void translate() throws Exception {
        output.printf("default rel\n");
        for (int i = 0; i < Enviroment.program.functions.size(); i ++) {
            output.printf("global %s\n", Enviroment.program.functions.get(i).name);
        }
        output.printf("extern printf, malloc, strcpy, scanf, strlen, sscanf, sprintf, memcpy, strcmp, puts\n");
        output.printf("SECTION .text\n");
        for (Function function : Enviroment.program.functions) {
            translate(function.graph);
            output.printf("\n");
        }
        output.printf("\n%s\n", BuiltinFunction.getBuiltinFunction());
    }
}