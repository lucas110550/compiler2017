package Translator.NASM.NASMTranslator;

import Allocate.PhysicalRegister;
import ControlFlowGraph.Block;
import ControlFlowGraph.Graph;
import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Instruction.BinaryInstruction.BinaryInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.ControlFlowInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.ReturnInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.MemoryInstruction.*;
import ControlFlowGraph.Instruction.UnaryInstruction.UnaryInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.StringRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TempRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.VariableRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Function;
import Translator.NASM.NASMRegister;

import java.io.*;


import java.io.PrintStream;

public class MtoMTranslator extends NASMTranslator {
    public Graph graph;

    public MtoMTranslator(PrintStream output) {
        super(output);
    }

    private void load(Operand from, PhysicalRegister to) {
        if (from instanceof ImmediateValue) {
            output.printf("\tmov %s, %s\n", to, from);
        } else if (from instanceof VirtualRegister) {
            if (from instanceof StringRegister) {
                output.printf("\tmov %s, %s\n", to, StringConstantName(from));
            } else if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister) {
                    output.printf("\tmov %s, %s\n", to, getGlobalVariableName(from));
                } else if (from instanceof TempRegister) {
                    output.printf("\tmov %s, qword [%s+%d]\n", to, NASMRegister.rsp, graph.frame.getOffset(from));
                } else if (from instanceof ParameterRegister) {
                    output.printf("\tmov %s, qword [%s+%d]\n", to, NASMRegister.rsp, graph.frame.getOffset(from));
                }
            }
        }
    }

    private void store(VirtualRegister from, PhysicalRegister to) {
        if (from instanceof StringRegister) {
            throw new InternalError();
        } else if (from instanceof VariableRegister) {
            if (from instanceof GlobalRegister) {
                output.printf("\tmov %s, %s\n", getGlobalVariableName(from), to);
            } else if (from instanceof TempRegister) {
                output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.getOffset(from), to);
            } else if (from instanceof ParameterRegister) {
                output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.rsp, graph.frame.getOffset(from), to);
            }
        }
    }

    @Override
    public void translate(Graph graph) {
        this.graph = graph;
        output.printf("%s:\n", getFunctionName(graph.function));
        output.printf("\tpush %s\n", NASMRegister.rbp);
        output.printf("\tmov %s, %s\n", NASMRegister.rbp, NASMRegister.rsp);
        output.printf("\tsub %s, %d\n", NASMRegister.rsp, graph.frame.size);
        int cnt = graph.frame.size;
        for (int k = 0; k < graph.blocks.size(); ++k) {
            Block block = graph.blocks.get(k);
            output.printf("\n%s:\n", getBlockName(block));
            for (Instruction instruction : block.instructions) {
//				output.printf("\n#\t%s\n", instruction);
                if (instruction instanceof ArithInstruction) {
                    if (instruction instanceof UnaryInstruction) {
                        UnaryInstruction i = (UnaryInstruction)instruction;
                        load(i.s, NASMRegister.r12);
                        output.printf("\t%s %s\n", i.name(), NASMRegister.r12);
                        store(i.d, NASMRegister.r12);
                    } else if (instruction instanceof BinaryInstruction) {
                        BinaryInstruction i = (BinaryInstruction)instruction;
                        if (i.name().equals("div") || i.name().equals("rem")) {
                            load(i.l, NASMRegister.rax);
                            load(i.r, NASMRegister.r12);
                            output.printf("\tcqo\n");
                            output.printf("\tidiv %s\n", NASMRegister.r12);
                            if (i.name().equals("rem")) output.printf("\tmov %s, %s\n", NASMRegister.rax, NASMRegister.rdx);
                        }
                        else if (i.name().equals("sal") || i.name().equals("sar")) {
                            load(i.r, NASMRegister.rax);
                            output.printf("\tmov edx, eax\n");
                            load(i.l, NASMRegister.rax);
                            output.printf("\tmov ecx, edx\n");
                            output.printf("\t%s %s, cl\n", i.name(), NASMRegister.rax);
                        }
                        else if (i.name().equals("g") || i.name().equals("ge") || i.name().equals("l") || i.name().equals("le") || i.name().equals("e") || i.name().equals("ne")) {
                            load(i.l, NASMRegister.rax);
                            load(i.r, NASMRegister.r12);
                            output.printf("\tcmp %s, %s\n", NASMRegister.rax, NASMRegister.r12);
                            output.printf("\tset%s al\n", i.name());
                            output.printf("\tmovzx eax, al\n");
                        }
                        else {
                            load(i.l, NASMRegister.rax);
                            load(i.r, NASMRegister.r12);
                            output.printf("\t%s %s, %s\n", i.name(), NASMRegister.rax, NASMRegister.r12);
                        }
                        store(i.d, NASMRegister.rax);
                    }
                } else if (instruction instanceof ControlFlowInstruction) {
                    if (instruction instanceof BranchInstruction) {
                        BranchInstruction i = (BranchInstruction)instruction;
                        load(i.cond, NASMRegister.r12);
                        output.printf("\tcmp %s, 1\n", NASMRegister.r12);
                        output.printf("\tjnz %s\n", getBlockName(i.FalseCond.block));
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.TrueCond.block) {
                            output.printf("\tjmp %s\n", getBlockName(i.TrueCond.block));
                        }
                    } else if (instruction instanceof JumpInstruction) {
                        JumpInstruction i = (JumpInstruction)instruction;
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.to.block) {
                            output.printf("\tjmp %s\n", getBlockName(i.to.block));
                        }
                    }
                } else if (instruction instanceof CallInstruction) {
                    CallInstruction i = (CallInstruction)instruction;
                    Function function = i.function;
                    if (function.name.startsWith("___")) {
                        if (i.parameter.size() >= 1) {
                            load(i.parameter.get(0), NASMRegister.rdi);
                        }
                        if (i.parameter.size() >= 2) {
                            load(i.parameter.get(1), NASMRegister.rsi);
                        }
                        if (i.parameter.size() >= 3) {
                            load(i.parameter.get(2), NASMRegister.rdx);
                        }
                        if (i.parameter.size() >= 4) {
                            load(i.parameter.get(3), NASMRegister.rcx);
                        }
                        if (cnt % 16 != 0) {
                            output.printf("\tsub %s, %d\n", NASMRegister.rsp, 8);
                            output.printf("\tcall %s\n", getFunctionName(function));
                            output.printf("\tadd %s, %d\n", NASMRegister.rsp, 8);
                        }
                        else output.printf("\tcall %s\n", getFunctionName(function));
                        if (i.d != null) store(i.d, NASMRegister.rax);
                    } else {
                        boolean mark = false;

                        for (int p = 0; p < function.parameters.size(); ++p) {
                            load(i.parameter.get(p), NASMRegister.r12);
                            int offset = function.graph.frame.size - function.graph.frame.getOffset(function.parameters.get(p).register);
                            int xx = 16;
                            if (cnt % 16 != 0) xx += 8;
                            output.printf("\tmov qword [%s-%d], %s\n", NASMRegister.rsp, offset + xx, NASMRegister.r12);
                        }
                        if (cnt % 16 != 0) {
                            output.printf("\tsub %s, %d\n", NASMRegister.rsp, 8);
                            mark = true;
                        }
                        output.printf("\tcall %s\n", getFunctionName(function));
                        if (mark) output.printf("\tadd %s, %d\n", NASMRegister.rsp, 8);
                        if (i.d != null) store(i.d, NASMRegister.rax);
                    }

                } else if (instruction instanceof ReturnInstruction) {
                    ReturnInstruction i = (ReturnInstruction)instruction;
                    load(i.s, NASMRegister.rax);
                    output.printf("\tleave\n\tret\n");
                  } else if (instruction instanceof MemoryInstruction) {
                    if (instruction instanceof AllocateInstruction) {
                        AllocateInstruction i = (AllocateInstruction) instruction;
                        load(i.size, NASMRegister.rdi);
                        if (cnt % 16 != 0) {
                            output.printf("\tsub %s, %d\n", NASMRegister.rsp, 8);
                            output.printf("\tcall malloc\n");
                            output.printf("\tadd %s, %d\n", NASMRegister.rsp, 8);
                        }
                        else output.printf("\tcall malloc\n");
                        store(i.d, NASMRegister.rax);
                    } else if (instruction instanceof LoadInstruction) {
                        LoadInstruction i = (LoadInstruction)instruction;
                        load(i.address.base, NASMRegister.rax);
                        output.printf("\tmov %s, qword [%s+%d]\n", NASMRegister.r12, NASMRegister.rax, i.address.offset.value);
                        store(i.d, NASMRegister.r12);
                    } else if (instruction instanceof MovInstruction) {
                        MovInstruction i = (MovInstruction)instruction;
                        load(i.s, NASMRegister.r12);
                        store(i.d, NASMRegister.r12);
                    } else if (instruction instanceof StoreInstruction) {
                        StoreInstruction i = (StoreInstruction)instruction;
                        load(i.s, NASMRegister.rax);
                        load(i.address.base, NASMRegister.r12);
                        output.printf("\tmov qword [%s+%d], %s\n", NASMRegister.r12, i.address.offset.value, NASMRegister.rax);
                    }
                }
            }
        }

        output.printf("\tadd %s, %d\n", NASMRegister.rsp, graph.frame.size);
        output.printf("\tpop %s\n", NASMRegister.rbp);
        output.printf("\tret\n");
    }
}