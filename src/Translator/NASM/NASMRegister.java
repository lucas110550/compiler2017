package Translator.NASM;

import Allocate.PhysicalRegister;

public class NASMRegister extends PhysicalRegister {
    public static PhysicalRegister rax = new NASMRegister(1, "rax");
    public static PhysicalRegister rdx = new NASMRegister(2, "rdx");
    public static PhysicalRegister rbx = new NASMRegister(3, "rbx");
    public static PhysicalRegister rcx = new NASMRegister(4, "rcx");

    public static PhysicalRegister rsp = new NASMRegister(5, "rsp");
    public static PhysicalRegister rbp = new NASMRegister(6, "rbp");
    public static PhysicalRegister r12 = new NASMRegister(7, "r12");
    public static PhysicalRegister r13 = new NASMRegister(8, "r13");

    public static PhysicalRegister rdi = new NASMRegister(9, "rdi");
    public static PhysicalRegister rsi = new NASMRegister(10, "rsi");
    public static PhysicalRegister r14 = new NASMRegister(11, "r14");
    public static PhysicalRegister r15 = new NASMRegister(12, "r15");
    public static PhysicalRegister r9 = new NASMRegister(13, "r9");
    public static PhysicalRegister r10 = new NASMRegister(14, "r10");
    public static PhysicalRegister r11 = new NASMRegister(15, "r11");

    public static PhysicalRegister r8 = new NASMRegister(16, "r8");
    public NASMRegister(int index, String name) {
        super(index, name);
    }
    public static int size() { return 8; }
}