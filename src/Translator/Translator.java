package Translator;

import ControlFlowGraph.Graph;

import java.io.*;

public class Translator {
    public PrintStream output;
    public Translator(PrintStream output) {
        this.output = output;
    }
    public void translate(Graph graph) {}
    public void translate() throws Exception {}
}