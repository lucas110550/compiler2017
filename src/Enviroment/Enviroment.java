package Enviroment;

import Scope.Scope;
import Scope.ScopeTable;
import Symbol.Symbol;
import Symbol.SymbolTable;
import Front.Function;
import Front.Program;
import Front.Type.BasicType.BoolType;
import Front.Type.BasicType.IntType;
import Front.Type.BasicType.StringType;
import Front.Type.BasicType.VoidType;

import java.util.ArrayList;

public class Enviroment {
    public static Program program;
    public static ScopeTable scopetable;
    public static SymbolTable symboltable;
    public static ClassTable classtable;
    public static RegisterTable registertable;

    public static void initialize() {
        classtable = new ClassTable();
        symboltable = new SymbolTable();
        scopetable = new ScopeTable();
        registertable = new RegisterTable();
        intoScope(program = Program.getProgram());
        LoadLibFunctions();
    }

    public static void intoScope(Scope scope) {
        scopetable.intoScope(scope);
        symboltable.intoScope();
    }

    public static void outScope() {
        symboltable.outScope();
        scopetable.outScope();
    }

    public static void LoadLibFunctions() {
        symboltable.add("print", Function.getFunction("___print", VoidType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("str", StringType.getType())); }}));
       //S symboltable.add("printInt", Function.getFunction("___printInt", VoidType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("int", IntType.getType())); }}));
        symboltable.add("println", Function.getFunction("___println", VoidType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("str", StringType.getType())); }}));
        symboltable.add("getString", Function.getFunction("___getString", StringType.getType(), new ArrayList<>()));
        symboltable.add("getInt", Function.getFunction("___getInt", IntType.getType(), new ArrayList<>()));
        symboltable.add("toString", Function.getFunction("___toString", StringType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("int", IntType.getType())); }}));
        symboltable.add("___array_size", Function.getFunction("___array_size", IntType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("this", VoidType.getType())); }}));
        symboltable.add("___string_length", Function.getFunction("___string_length", IntType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("this", StringType.getType())); }}));
        symboltable.add("___string_ord", Function.getFunction("___string_ord", IntType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("this", StringType.getType()));add(new Symbol("pos", IntType.getType())); }}));
        symboltable.add("___string_substring", Function.getFunction("___string_substring", StringType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("this", StringType.getType())); add(new Symbol("lhs", IntType.getType())); add(new Symbol("rhs", IntType.getType())); }}));
        symboltable.add("___string_parseInt", Function.getFunction("___string_parseInt", IntType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("this", StringType.getType())); }}));
        symboltable.add("___string_concatenate", Function.getFunction("___string_concatenate", StringType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
        symboltable.add("___string_equalto", Function.getFunction("___string_equalto", BoolType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
        symboltable.add("___string_greaterthan", Function.getFunction("___string_greaterthan", BoolType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
        symboltable.add("___string_geq", Function.getFunction("___string_geq", BoolType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
        symboltable.add("___string_lessthan", Function.getFunction("___string_lessthan", BoolType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
        symboltable.add("___string_leq", Function.getFunction("___string_leq", BoolType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
        symboltable.add("___string_neq", Function.getFunction("___string_neq", BoolType.getType(), new ArrayList<Symbol>() {{ add(new Symbol("lhs", StringType.getType())); add(new Symbol("rhs", StringType.getType())); }}));
    }
}