package Enviroment;

import Front.Type.ClassType.ClassType;
import exception.CompileError;

import java.util.*;

public class ClassTable {
    private Map<String, ClassType> allclass;

    public ClassTable() {
        allclass = new HashMap<>();
    }

    public void put(String name, ClassType type) {
        if (allclass.containsKey(name)) {
            throw new CompileError("Two classes have the same name.");
        }
        allclass.put(name, type);
    }

    public ClassType get(String name) {
        return allclass.get(name);
    }

    public boolean contains(String name) {
        return allclass.containsKey(name);
    }
}