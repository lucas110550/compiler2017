package Enviroment;

import ControlFlowGraph.Operand.VirtualRegister.StringRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TempRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Symbol.Symbol;
import java.util.*;

public class RegisterTable {
    public Set<VirtualRegister> registers;
    public RegisterTable() {
        registers = new HashSet<>();
    }
    public VirtualRegister addTempRegister(Symbol symbol) {
        VirtualRegister register = new TempRegister(symbol);
        registers.add(register);
        return register;
    }
    public VirtualRegister addTempRegister() {
        return addTempRegister(null);
    }
    public VirtualRegister addStringRegister(String value) {
        VirtualRegister register = new StringRegister(value);
        registers.add(register);
        return register;
    }
    public VirtualRegister addGlobalRegister(Symbol value) {
        VirtualRegister register = new GlobalRegister(value);
        registers.add(register);
        return register;
    }
    public VirtualRegister addParameterRegister(Symbol value) {
        VirtualRegister register = new ParameterRegister(value);
        registers.add(register);
        return register;
    }
}