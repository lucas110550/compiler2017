package ControlFlowGraph;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Function;
import Utility.Util;

import java.util.*;
public class Block {
    public List<Instruction> instructions;
    public Function function;
    public String name;
    public int index;
    public LabelInstruction labelInstruction;
    public List<Block> pred, succ;
    public List<VirtualRegister> used, defined;
    public Set<VirtualRegister> LiveIn, LiveOut;

    public Block(Function function, String name, int index, LabelInstruction labelInstruction) {
        this.function = function;
        this.name = name;
        this.index = index;
        this.labelInstruction = labelInstruction;
        this.instructions = new ArrayList<>();
        this.pred = new ArrayList<>();
        this.succ = new ArrayList<>();
        this.used = new ArrayList<>();
        this.defined = new ArrayList<>();
        this.LiveIn = new HashSet<>();
        this.LiveOut = new HashSet<>();
    }
    public Set<VirtualRegister> getallRegisters() {
        Set<VirtualRegister> x = new HashSet<>();
        for (Instruction instruction : instructions) {
            x.addAll(instruction.ExistRegister());
            x.addAll(instruction.UsedRegister());
        }
        return x;
    }
    @Override
    public String toString() {
        return "%" + function.name + "." + index + "." + name;
    }

    public String toString(int indents) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Util.getindent(indents)).append(this).append(":").append("\n");
        instructions.forEach(instruction -> stringBuilder.append(instruction.toString(indents + 1)));
        return stringBuilder.toString();
    }
}