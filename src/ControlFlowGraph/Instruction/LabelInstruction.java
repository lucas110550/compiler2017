package ControlFlowGraph.Instruction;

import ControlFlowGraph.Block;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import ControlFlowGraph.Block;
import java.util.*;

public class LabelInstruction extends Instruction {
    public String name;
    public Block block;
    public LabelInstruction(String name) {
        this.name = name;
    }
    public static LabelInstruction getInstruction(String name) {
        return new LabelInstruction(name);
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {}
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {}
    @Override
    public String toString() { return "%" + name; }
}