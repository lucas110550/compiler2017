package ControlFlowGraph.Instruction;

import Utility.Util;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import java.util.*;

public abstract class Instruction {
    public Instruction Simplify() { return this; }
    public abstract List<Operand> ExistOperand();
    public abstract List<Operand> UsedOperand();
    public abstract void setExistRegister(VirtualRegister from, VirtualRegister to);
    public abstract void setUsedRegister(VirtualRegister from, Operand to);
    public List<VirtualRegister> ExistRegister() {
        ArrayList<VirtualRegister> x = new ArrayList<VirtualRegister>();
        for (Operand operand : ExistOperand())
            if (operand instanceof VirtualRegister)
                x.add((VirtualRegister)operand);
        return x;
    }
    public List<VirtualRegister> UsedRegister() {
        ArrayList<VirtualRegister> x = new ArrayList<VirtualRegister>();
        for (Operand operand : UsedOperand())
            if (operand instanceof VirtualRegister)
                x.add((VirtualRegister)operand);
        return x;
    }
    public String toString(int indents) {
        StringBuilder str = new StringBuilder();
        str.append(Util.getindent(indents)).append(this).append("\n");
        return str.toString();
    }
}