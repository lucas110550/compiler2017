package ControlFlowGraph.Instruction.MemoryInstruction;

import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.Operand;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import exception.InternalError;

import java.util.*;

public class AllocateInstruction extends MemoryInstruction {
    public VirtualRegister d;
    public Operand size;

    public AllocateInstruction(VirtualRegister d, Operand size) {
        this.size = size;
        this.d = d;
    }
    public static Instruction getInstruction(Operand d, Operand size) {
        if (d instanceof VirtualRegister)
            return new AllocateInstruction((VirtualRegister)d, size);
        throw new InternalError();
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(d);
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(size);
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
        if (d == from) d = to;
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (size == from) size = to;
    }
    @Override
    public String toString() {
        return d + " = alloc " + size;
    }
}