package ControlFlowGraph.Instruction.MemoryInstruction;

import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.Operand;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import exception.InternalError;
import ControlFlowGraph.Operand.Address;

import java.util.*;

public class LoadInstruction extends MemoryInstruction {
    public Address address;
    public VirtualRegister d;

    public LoadInstruction(VirtualRegister d, Address address) {
        this.address = address;
        this.d = d;
    }
    public static Instruction getInstruction(Operand d, Operand address) {
        if (d instanceof VirtualRegister && address instanceof Address)
            return new LoadInstruction((VirtualRegister)d, (Address)address);
        throw new InternalError();
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(d);
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(address.base);
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
        if (d == from) d = to;
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (address.base == from)
            address = new Address((VirtualRegister)to, address.offset, address.size);
    }
    @Override
    public String toString() {
        return d + " = load " + address.size + " " + address.base + " " + address.offset;
    }
}