package ControlFlowGraph.Instruction.MemoryInstruction;

import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.Operand;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import exception.InternalError;

import java.util.*;

public class MovInstruction extends MemoryInstruction {
    public VirtualRegister d;
    public Operand s;

    public MovInstruction(VirtualRegister d, Operand s) {
        this.d = d;
        this.s = s;
    }
    public static Instruction getInstruction(Operand dest, Operand s) {
        if (dest instanceof VirtualRegister)
            return new MovInstruction((VirtualRegister)dest, s);
        throw new InternalError();
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(d);
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(s);
        return x;
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (s == from) s = to;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
        if (d == from) d = to;
    }
    @Override
    public String toString() {
        return d + " = move " + s;
    }
}