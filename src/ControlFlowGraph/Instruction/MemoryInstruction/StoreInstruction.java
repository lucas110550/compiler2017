package ControlFlowGraph.Instruction.MemoryInstruction;

import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.Operand;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import exception.InternalError;
import ControlFlowGraph.Operand.Address;

import java.util.*;

public class StoreInstruction extends MemoryInstruction {
    public Address address;
    public Operand s;

    public StoreInstruction(Operand s, Address address) {
        this.address = address;
        this.s = s;
    }
    public static Instruction getInstruction(Operand s, Operand address) {
        if (address instanceof Address)
            return new StoreInstruction(s, (Address)address);
        throw new InternalError();
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(s); x.add(address.base);
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (s == from) s = to;
        if (address.base == from)
            address = new Address((VirtualRegister)to, address.offset, address.size);
    }
    @Override
    public String toString() {
        return "store " + address.size + " " + address.base + " " + s + " " + address.offset;
    }
}