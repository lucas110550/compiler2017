package ControlFlowGraph.Instruction.UnaryInstruction;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import exception.InternalError;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

public class UnaryMinusInstruction extends UnaryInstruction {
    public UnaryMinusInstruction(VirtualRegister d, Operand s) {
        super(d, s);
    }
    public static Instruction getInstruction(Operand d, Operand s) {
        if (d instanceof VirtualRegister)
            return new UnaryMinusInstruction((VirtualRegister)d, s).Simplify();
        else throw new InternalError();
    }
    @Override
    public Instruction Simplify() {
        if (s instanceof ImmediateValue) {
            int x = ((ImmediateValue) s).value;
            return MovInstruction.getInstruction(d, new ImmediateValue(-x));
        }
        return this;
    }
    @Override
    public String name() {
        return "neg";
    }
    @Override
    public String toString() {
        return String.format("%s = neg %s", d, s);
    }
}