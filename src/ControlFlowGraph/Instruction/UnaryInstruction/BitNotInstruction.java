package ControlFlowGraph.Instruction.UnaryInstruction;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import exception.InternalError;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

public class BitNotInstruction extends UnaryInstruction {
    public BitNotInstruction(VirtualRegister d, Operand s) {
        super(d, s);
    }
    public static Instruction getInstruction(Operand d, Operand s) {
        if (d instanceof VirtualRegister)
            return new BitNotInstruction((VirtualRegister)d, s).Simplify();
        else throw new InternalError();
    }
    @Override
    public Instruction Simplify() {
        if (s instanceof ImmediateValue) {
            int x = ((ImmediateValue) s).value;
            return MovInstruction.getInstruction(d, new ImmediateValue(~x));
        }
        return this;
    }
    @Override
    public String name() {
        return "not";
    }
    @Override
    public String toString() {
        return String.format("%s = not %s", d, s);
    }
}