package ControlFlowGraph.Instruction.UnaryInstruction;

import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import java.util.*;

public abstract class UnaryInstruction extends ArithInstruction {
    public VirtualRegister d;
    public Operand s;

    public UnaryInstruction(VirtualRegister d, Operand s) {
        this.d = d;
        this.s = s;
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(d);
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(s);
        return x;
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (s == from) s = to;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
        if (d == from) d = to;
    }
}