package ControlFlowGraph.Instruction.FunctionInstruction;

import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import Enviroment.Enviroment;

import Front.Function;
import Front.Statement.VariableDeclarationStatement;
import exception.InternalError;

import java.util.*;

public class ReturnInstruction extends FunctionInstruction {
    public Operand s;
    public ReturnInstruction(Operand s) {
        this.s = s;
    }
    public static Instruction getInstruction(Operand s) {
        return new ReturnInstruction(s);
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(s);
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {}
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (s == from) s = to;
    }
    @Override
    public String toString() {
        return "ret " + s;
    }
}