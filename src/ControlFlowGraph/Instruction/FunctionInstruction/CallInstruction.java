package ControlFlowGraph.Instruction.FunctionInstruction;

import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import Enviroment.Enviroment;

import Front.Function;
import Front.Statement.VariableDeclarationStatement;
import exception.InternalError;

import java.util.*;

public class CallInstruction extends FunctionInstruction {
    public VirtualRegister d;
    public Function function;
    public List<Operand> parameter;

    public CallInstruction(VirtualRegister d, Function function, List<Operand> parameter) {
        this.d = d;
        this.function = function;
        this.parameter = parameter;
    }
    public static Instruction getInstruction(Operand to, Function function, List<Operand> parameter) {
        if (to == null) return new CallInstruction(null, function, parameter);
        if (to instanceof VirtualRegister)
            return new CallInstruction((VirtualRegister)to, function, parameter);
        throw new InternalError();
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        if (d != null) x.add(d);
        if (!function.name.startsWith("___")) {
            for (VariableDeclarationStatement variableDeclarationStatement : Enviroment.program.variables) {
                x.add(variableDeclarationStatement.symbol.register);
            }
        }
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.addAll(parameter);
        if (!function.name.startsWith("___")) {
            for (VariableDeclarationStatement variableDeclarationStatement: Enviroment.program.variables) {
                x.add(variableDeclarationStatement.symbol.register);
            }
        }
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
        if (d == from) d = to;
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        for (int i = 0; i < parameter.size(); i ++)
            if (parameter.get(i) == from) parameter.set(i, to);
    }
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (d != null) {
            stringBuilder.append(d).append(" = ");
        }
        stringBuilder.append("call ").append(function.name);
        parameter.forEach(x -> stringBuilder.append(" ").append(x));
        return stringBuilder.toString();
    }
}