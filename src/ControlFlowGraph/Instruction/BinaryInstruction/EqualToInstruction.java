package ControlFlowGraph.Instruction.BinaryInstruction;

import ControlFlowGraph.Instruction.Instruction;
import exception.InternalError;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

public class EqualToInstruction extends BinaryInstruction {
    public EqualToInstruction(VirtualRegister d, Operand l, Operand r) {
        super(d, l, r);
    }
    public static Instruction getInstruction(Operand d, Operand l, Operand r) {
        if (d instanceof VirtualRegister)
            return new EqualToInstruction((VirtualRegister)d, l, r).Simplify();
        else throw new InternalError();
    }
    @Override
    public Instruction Simplify() {
        return this;
    }
    @Override
    public String name() {
        return "e";
    }
    @Override
    public String toString() {
        return String.format("%s = seq %s %s", d, l, r);
    }
}