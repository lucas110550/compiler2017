package ControlFlowGraph.Instruction.BinaryInstruction;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import exception.InternalError;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

public class BitAndInstruction extends BinaryInstruction {
    public BitAndInstruction(VirtualRegister d, Operand l, Operand r) {
        super(d, l, r);
    }
    public static Instruction getInstruction(Operand d, Operand l, Operand r) {
        if (d instanceof VirtualRegister)
            return new BitAndInstruction((VirtualRegister)d, l, r).Simplify();
        else throw new InternalError();
    }
    @Override
    public Instruction Simplify() {
        if (l instanceof ImmediateValue) {
            if (r instanceof ImmediateValue) {
                int x = ((ImmediateValue) l).value;
                int y = ((ImmediateValue) r).value;
                return MovInstruction.getInstruction(d, new ImmediateValue(x & y));
            }
        }
        return this;
    }
    @Override
    public String name() {
        return "and";
    }
    @Override
    public String toString() {
        return String.format("%s = and %s %s", d, l, r);
    }
}