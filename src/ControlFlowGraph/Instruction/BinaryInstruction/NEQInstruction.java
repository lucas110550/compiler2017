package ControlFlowGraph.Instruction.BinaryInstruction;

import ControlFlowGraph.Instruction.Instruction;
import exception.InternalError;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

public class NEQInstruction extends BinaryInstruction {
    public NEQInstruction(VirtualRegister d, Operand l, Operand r) {
        super(d, l, r);
    }
    public static Instruction getInstruction(Operand d, Operand l, Operand r) {
        if (d instanceof VirtualRegister)
            return new NEQInstruction((VirtualRegister)d, l, r).Simplify();
        else throw new InternalError();
    }
    @Override
    public Instruction Simplify() {
        return this;
    }
    @Override
    public String name() {
        return "ne";
    }
    @Override
    public String toString() {
        return String.format("%s = sne %s %s", d, l, r);
    }
}