package ControlFlowGraph.Instruction.BinaryInstruction;

import ControlFlowGraph.Instruction.ArithInstruction;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import java.util.*;

public abstract class BinaryInstruction extends ArithInstruction {
    public VirtualRegister d;
    public Operand l, r;

    public BinaryInstruction(VirtualRegister d, Operand l, Operand r) {
        this.d = d;
        this.l = l;
        this.r = r;
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(d);
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(l); x.add(r);
        return x;
    }
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (l == from) l = to;
        if (r == from) r = to;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {
        if (d == from) d = to;
    }
}