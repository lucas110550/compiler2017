package ControlFlowGraph.Instruction;

public abstract class ArithInstruction extends Instruction{
    public abstract String name();
}