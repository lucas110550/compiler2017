package ControlFlowGraph.Instruction.ControlFlowInstruction;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import ControlFlowGraph.Operand.ImmediateValue;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import java.util.*;
import java.util.concurrent.locks.Condition;

public class BranchInstruction extends ControlFlowInstruction {
    public Operand cond;
    public LabelInstruction TrueCond, FalseCond;
    public BranchInstruction(Operand cond, LabelInstruction TrueCond, LabelInstruction FalseCond) {
        this.cond = cond;
        this.TrueCond = TrueCond;
        this.FalseCond = FalseCond;
    }
    public static Instruction getInstruction(Operand cond, LabelInstruction TrueCond, LabelInstruction FalseCond) {
        return new BranchInstruction(cond, TrueCond, FalseCond).Simplify();
    }
    @Override
    public Instruction Simplify() {
        if (cond instanceof ImmediateValue) {
            int tmp = ((ImmediateValue)cond).value;
            if (tmp == 1) return JumpInstruction.getInstruction(TrueCond);
            else return JumpInstruction.getInstruction(FalseCond);
        }
        return this;
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        x.add(cond);
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {}
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
        if (cond == from) cond = to;
    }
    @Override
    public String toString() {
        return "br" + cond + " " + TrueCond.block + " " + FalseCond.block;
    }
}
