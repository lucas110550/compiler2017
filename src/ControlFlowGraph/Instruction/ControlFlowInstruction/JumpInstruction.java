package ControlFlowGraph.Instruction.ControlFlowInstruction;

import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import java.util.*;

public class JumpInstruction extends ControlFlowInstruction {
    public LabelInstruction to;
    public JumpInstruction(LabelInstruction to) {
        this.to = to;
    }
    public static Instruction getInstruction(LabelInstruction x) {
        return new JumpInstruction(x);
    }
    @Override
    public List<Operand> ExistOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public List<Operand> UsedOperand() {
        List<Operand> x = new ArrayList<>();
        return x;
    }
    @Override
    public void setExistRegister(VirtualRegister from, VirtualRegister to) {}
    @Override
    public void setUsedRegister(VirtualRegister from, Operand to) {
    }
    @Override
    public String toString() { return "Jump " + to.block; }
}