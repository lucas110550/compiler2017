package ControlFlowGraph;

import ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.ControlFlowInstruction;
import ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.LabelInstruction;
import ControlFlowGraph.Operand.Operand;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TempRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Function;
import Front.Statement.VariableDeclarationStatement;
import Enviroment.Enviroment;
import Front.Type.BasicType.VoidType;
import Symbol.Symbol;
import Utility.Util;
import exception.InternalError;

import java.util.*;
public class Graph {
    public Function function;
    public List<Block> blocks;
    public Block enter, subStatement, out;
    public Frame frame;

    public Graph(Function function) {
        this.function = function;
        this.build();
    }
    public void build() {
        List<Instruction> instructions = new ArrayList<>();
        function.enter = LabelInstruction.getInstruction("enter");
        function.subStatement = LabelInstruction.getInstruction("subStatement");
        function.out = LabelInstruction.getInstruction("out");
        instructions.add(function.enter);
        if (function.name.equals("main")) {
            for (VariableDeclarationStatement variableDeclarationStatement : Enviroment.program.variables)
                variableDeclarationStatement.combine(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(function.subStatement));
        instructions.add(function.subStatement);
        function.statements.combine(instructions);
        instructions.add(JumpInstruction.getInstruction(function.out));
        instructions.add(function.out);
        blocks = new ArrayList<>();
        for (int i = 0; i < instructions.size(); i ++) {
            if (!(instructions.get(i) instanceof LabelInstruction)) continue;
            LabelInstruction tmp = (LabelInstruction)(instructions.get(i));
            int k = i + 1;
            Block block = tmp.block = addBlock(tmp.name, tmp);
            while (k < instructions.size()) {
                if (instructions.get(k) instanceof LabelInstruction) break;
                block.instructions.add(instructions.get(k));
                if (instructions.get(k) instanceof ControlFlowInstruction) break;
                k ++;
            }
            -- k;
            i = k;
        }
        for (Block block : blocks) {
            if (block.name.equals("enter")) enter = block;
            else if (block.name.equals("subStatement")) subStatement = block;
            else if (block.name.equals("out")) out = block;
        }
        rebuild();
    }
    public Graph rebuild() {
        LoadInstructions();
        InitGraph();
        Liveliness();
        makeFrame();
        return this;
    }
    public void LoadInstructions() {
        for (Block block : blocks) {
            List<Instruction> instructions = block.instructions;
            block.instructions = new ArrayList<>();
            for (Instruction instruction : instructions)
                if (instruction != null) block.instructions.add(instruction);
        }
    }
    public void InitGraph() {
        for (Block block : blocks) {
            block.pred = new ArrayList<>();
            block.succ = new ArrayList<>();
        }
        for (Block block : blocks) {
            if (block.instructions.isEmpty() == false) {
                Instruction tmp = block.instructions.get(block.instructions.size() - 1);
                if (tmp instanceof JumpInstruction) {
                    JumpInstruction x = (JumpInstruction) tmp;
                    block.succ.add(x.to.block);
                } else if (tmp instanceof BranchInstruction) {
                    BranchInstruction x = (BranchInstruction) tmp;
                    block.succ.add(x.TrueCond.block);
                    block.succ.add(x.FalseCond.block);
                }
            }
        }
        blocks = dfs(enter, new HashSet<>());
        for (Block block : blocks)
            for (Block nxt : block.succ)
                nxt.pred.add(block);
    }
    public List<Block> dfs(Block x, Set<Block> vis) {
        vis.add(x);
        List<Block> ans = new ArrayList<>();
        ans.add(x);
        for (Block block : x.succ) {
            if (vis.contains(block)) continue;
            if (block != out) {
                vis.add(block);
                ans.addAll(dfs(block, vis));
            }
        }
        if (x == enter) ans.add(out);
        return ans;
    }
    public void Liveliness() {
        for (Block block : blocks) {
            block.used = new ArrayList<>();
            block.defined = new ArrayList<>();
            for (Instruction instruction : block.instructions) {
                for (VirtualRegister virtualRegister : instruction.UsedRegister())
                    if (!block.defined.contains(virtualRegister)) block.used.add(virtualRegister);
                for (VirtualRegister virtualRegister : instruction.ExistRegister())
                    block.defined.add(virtualRegister);
            }
        }
        for (Block block : blocks) {
            block.LiveIn = new HashSet<>();
            block.LiveOut = new HashSet<>();
        }
        while (true) {
            for (Block block : blocks) {
                block.LiveIn = new HashSet<VirtualRegister>();
                for (VirtualRegister tmp : block.LiveOut)
                    block.LiveIn.add(tmp);
                for (VirtualRegister tmp : block.defined)
                    block.LiveIn.remove(tmp);
                for (VirtualRegister tmp : block.used)
                    block.LiveIn.add(tmp);
            }
            boolean mark = false;
            for (Block block : blocks) {
                Set<VirtualRegister> x = block.LiveOut;
                block.LiveOut = new HashSet<VirtualRegister>();
                for (Block tmp : block.succ)
                    block.LiveOut.addAll(tmp.LiveIn);
                if (block.LiveOut.equals(x) == false) mark = true;
            }
            if (mark == false) break;
        }
    }
    public void makeFrame() {
        Set<VirtualRegister> registers = new HashSet<VirtualRegister>();
        for (Block block : blocks) {
            for (Instruction instruction : block.instructions) {
                for (VirtualRegister virtualRegister : instruction.UsedRegister())
                    if (virtualRegister instanceof TempRegister) registers.add(virtualRegister);
                for (VirtualRegister virtualRegister : instruction.ExistRegister())
                    if (virtualRegister instanceof TempRegister) registers.add(virtualRegister);
            }
        }
        frame = new Frame();
        frame.size += 8;
        for (Symbol symbol : function.parameters) {
            frame.parameter.put(symbol.register, frame.size);
            frame.size += 8;
        }
        for (VirtualRegister virtualRegister : registers) {
            frame.temp.put(virtualRegister, frame.size);
            frame.size += 8;
        }
        frame.size += 8;
        frame.size += 128;
    }
    public Block addBlock(String name, LabelInstruction labelInstruction) {
        Block block = new Block(function, name, blocks.size(), labelInstruction);
        blocks.add(block);
        return block;
    }
    public Set<VirtualRegister> getallRegisters() {
        Set<VirtualRegister> x = new HashSet<VirtualRegister>();
        for (Block block : blocks) x.addAll(block.getallRegisters());
        return x;
    }
    public boolean containsCall() {
        for (Block block : blocks)
            for (Instruction instruction : block.instructions)
                if (instruction instanceof CallInstruction) return true;
        return false;
    }
    public String toString(int indents) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Util.getindent(indents));
        if (function.type instanceof VoidType) {
            stringBuilder.append("void");
        } else {
            stringBuilder.append("func");
        }
        stringBuilder.append(" ").append(function.name);
        function.parameters.forEach(parameter -> stringBuilder.append(" ").append(parameter.register));
        stringBuilder.append(" {");
        blocks.forEach(block -> stringBuilder.append("\n").append(block.toString(indents + 1)));
        stringBuilder.append(Util.getindent(indents)).append("}").append("\n");
        return stringBuilder.toString();
    }
    public class Frame {
        public int size;
        public Map<VirtualRegister, Integer> temp, parameter;
        public Frame() {
            size = 0;
            temp = new HashMap<>();
            parameter = new HashMap<>();
        }
        public int getOffset(Operand operand) {
            if (operand instanceof VirtualRegister) {
                if (temp.containsKey(operand)) return temp.get(operand);
                if (parameter.containsKey(operand)) return parameter.get(operand);
            }
            throw new InternalError();
        }
    }
}