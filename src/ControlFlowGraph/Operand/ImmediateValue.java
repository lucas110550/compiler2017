package ControlFlowGraph.Operand;

public class ImmediateValue extends Operand {
    public int value;
    public ImmediateValue(int value) {
        this.value = value;
    }
    @Override
    public String toString() { return String.valueOf(value); }
    @Override
    public boolean equals(Object object) {
        if (object instanceof ImmediateValue) {
            ImmediateValue tmp = (ImmediateValue)object;
            return tmp.value == value;
        }
        return false;
    }
}