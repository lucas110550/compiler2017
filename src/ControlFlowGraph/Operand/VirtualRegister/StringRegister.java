package ControlFlowGraph.Operand.VirtualRegister;

public class StringRegister extends VirtualRegister {
    public String value;
    public StringRegister(String value) { this.value = value; }
}