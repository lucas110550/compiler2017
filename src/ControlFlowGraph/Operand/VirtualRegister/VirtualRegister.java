package ControlFlowGraph.Operand.VirtualRegister;

import ControlFlowGraph.Operand.Operand;
import Enviroment.Enviroment;

import java.util.*;

public abstract class VirtualRegister extends Operand {
    public int index;

    public VirtualRegister() {
        this.index = Enviroment.registertable.registers.size();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("$%d", index));
        return str.toString();
    }
}