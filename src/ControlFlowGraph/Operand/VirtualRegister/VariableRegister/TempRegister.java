package ControlFlowGraph.Operand.VirtualRegister.VariableRegister;

import Symbol.Symbol;
public class TempRegister extends VariableRegister {
    public TempRegister(Symbol symbol) {
        super(symbol);
    }
    @Override
    public String toString() {
        return String.format("$t%s", index);
    }
}