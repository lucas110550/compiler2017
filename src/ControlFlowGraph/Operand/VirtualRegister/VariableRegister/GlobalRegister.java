package ControlFlowGraph.Operand.VirtualRegister.VariableRegister;

import Symbol.Symbol;
public class GlobalRegister extends VariableRegister {
    public GlobalRegister(Symbol symbol) {
        super(symbol);
    }
    @Override
    public String toString() {
        return String.format("$g%s(%s)", index, symbol.name);
    }
}