package ControlFlowGraph.Operand.VirtualRegister.VariableRegister;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Symbol.Symbol;
public class VariableRegister extends VirtualRegister {
    public Symbol symbol;
    public VariableRegister(Symbol symbol) {
        this.symbol = symbol;
    }
}