package ControlFlowGraph.Operand.VirtualRegister.VariableRegister;

import Symbol.Symbol;
public class ParameterRegister extends VariableRegister {
    public ParameterRegister(Symbol symbol) {
        super(symbol);
    }
    @Override
    public String toString() {
        return String.format("$p%s", index);
    }
}