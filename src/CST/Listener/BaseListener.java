package CST.Listener;

import Front.Node;
import CST.Parser.MargatroidBaseListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

public abstract class BaseListener extends MargatroidBaseListener {
    public static int r, c;
    static ParseTreeProperty<Node> returnNode = new ParseTreeProperty<>();

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        r = ctx.getStart().getLine();
        c = ctx.getStart().getCharPositionInLine();
    }
}