package CST.Listener;

import CST.Parser.MargatroidBaseListener;
import Enviroment.Enviroment;
import Symbol.Symbol;
import Front.Expression.BinaryExpression.*;
import Front.Expression.ConstantExpression.*;
import Front.Expression.Expression;
import Front.Expression.FunctionCallExpression;
import Front.Expression.NewExpression;
import Front.Expression.UnaryExpression.*;
import Front.Expression.VariableExpression.*;
import Front.Function;
import Front.Statement.*;
import Front.Type.ClassType.*;
import Front.Type.Type;
import com.sun.org.apache.xpath.internal.operations.Or;
import exception.CompileError;
import exception.InternalError;
import jdk.nashorn.internal.ir.BlockStatement;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import CST.Parser.MargatroidParser;

import java.util.*;

public class BuildASTListener extends BaseListener {
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterProgram(MargatroidParser.ProgramContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitProgram(MargatroidParser.ProgramContext ctx) {
        for (int i = 0; i < ctx.variableDeclarationStatement().size(); i ++) {
            VariableDeclarationStatement tmp = (VariableDeclarationStatement)returnNode.get(ctx.variableDeclarationStatement(i));
            Enviroment.program.addVariable(tmp);
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterClassDeclaration(MargatroidParser.ClassDeclarationContext ctx) {
        ClassType classType = (ClassType)returnNode.get(ctx);
        Enviroment.intoScope(classType);
        classType.memberVariables.forEach((name, member) -> Enviroment.symboltable.add(name, member.type));
        classType.memberFunctions.forEach((name, member) -> Enviroment.symboltable.add(name, member.function));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitClassDeclaration(MargatroidParser.ClassDeclarationContext ctx) {
        ClassType classType = (ClassType)returnNode.get(ctx);
        ctx.variableDeclarationStatement().forEach( tmp -> {
            String name = tmp.Identifier().getText();
            if (tmp.expression() != null) {
                Member x = classType.getMember(name);
                if (x instanceof MemberVariable) {
                    MemberVariable memberVariable = (MemberVariable)x;
                    memberVariable.expression = (Expression)returnNode.get(tmp.expression());
                }
            }
        });
        Enviroment.outScope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterFunctionDeclaration(MargatroidParser.FunctionDeclarationContext ctx) {
        Function function = (Function)returnNode.get(ctx);
        Enviroment.intoScope(function);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFunctionDeclaration(MargatroidParser.FunctionDeclarationContext ctx) {
        Function function = (Function)returnNode.get(ctx);
        function.addStatements((CompondStatement)returnNode.get(ctx.compondStatement()));
        Enviroment.outScope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVariableDeclarationStatement(MargatroidParser.VariableDeclarationStatementContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVariableDeclarationStatement(MargatroidParser.VariableDeclarationStatementContext ctx) {
        if (!(ctx.parent instanceof MargatroidParser.ClassDeclarationContext)) {
            Type type = (Type)returnNode.get(ctx.type());
            String name = ctx.Identifier().getText();
            Symbol symbol;
            if (Enviroment.scopetable.getScope() == Enviroment.program) {
                symbol = Enviroment.symboltable.addGlobalVariable(name, type);
            } else {
                symbol = Enviroment.symboltable.addTempVariable(name, type);
            }
            Expression expression = (Expression)returnNode.get(ctx.expression());
            returnNode.put(ctx, VariableDeclarationStatement.getStatement(symbol, expression));
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStatement(MargatroidParser.StatementContext ctx) {
        if (ctx.parent instanceof MargatroidParser.IfStatementContext)
            Enviroment.intoScope(null);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStatement(MargatroidParser.StatementContext ctx) {
        if (ctx.parent instanceof MargatroidParser.IfStatementContext)
            Enviroment.outScope();
        returnNode.put(ctx, returnNode.get(ctx.getChild(0)));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterCompondStatement(MargatroidParser.CompondStatementContext ctx) {
        CompondStatement compondStatement = (CompondStatement)CompondStatement.getStatement();
        Enviroment.intoScope(compondStatement);
        if (ctx.parent instanceof MargatroidParser.FunctionDeclarationContext) {
            Function function = (Function)returnNode.get(ctx.parent);
            for (int i = 0; i < function.parameters.size(); i ++) {
                Symbol tmp = function.parameters.get(i);
                function.parameters.set(i, Enviroment.symboltable.addParameterVariable(tmp.name, tmp.type));
            }
        }
        returnNode.put(ctx, compondStatement);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitCompondStatement(MargatroidParser.CompondStatementContext ctx) {
        ctx.statement().forEach(tmp -> {
            ((CompondStatement)returnNode.get(ctx)).add((Statement)returnNode.get(tmp));
        });
        Enviroment.outScope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterExpressionStatement(MargatroidParser.ExpressionStatementContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitExpressionStatement(MargatroidParser.ExpressionStatementContext ctx) {
        returnNode.put(ctx, ExpressionStatement.getStatement((Expression)returnNode.get(ctx.expression())));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIfStatement(MargatroidParser.IfStatementContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIfStatement(MargatroidParser.IfStatementContext ctx) {
        returnNode.put(ctx, IfStatement.getStatement((Expression)returnNode.get(ctx.expression()), (Statement)returnNode.get(ctx.statement(0)), (Statement)returnNode.get(ctx.statement(1))));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterWhileStatement(MargatroidParser.WhileStatementContext ctx) {
        WhileStatement whileStatement = (WhileStatement)WhileStatement.getStatement();
        Enviroment.intoScope(whileStatement);
        returnNode.put(ctx, whileStatement);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitWhileStatement(MargatroidParser.WhileStatementContext ctx) {
        WhileStatement whileStatement = (WhileStatement)returnNode.get(ctx);
        whileStatement.addCondition((Expression)returnNode.get(ctx.expression()));
        whileStatement.addStatement((Statement)returnNode.get(ctx.statement()));
        Enviroment.outScope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterForStatement(MargatroidParser.ForStatementContext ctx) {
        ForStatement forStatement = (ForStatement)ForStatement.getStatement();
        Enviroment.intoScope(forStatement);
        returnNode.put(ctx, forStatement);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitForStatement(MargatroidParser.ForStatementContext ctx) {
        ForStatement forStatement = (ForStatement)returnNode.get(ctx);
        forStatement.addStatement((Statement)returnNode.get(ctx.statement()));
        int cnt = 0;
        for (ParseTree x : ctx.children) {
            if (x.getText().equals(";")) ++ cnt;
            if (x instanceof MargatroidParser.ExpressionContext) {
                Expression exp = (Expression)returnNode.get(x);
                if (cnt == 0) forStatement.Initialization(exp);
                else if (cnt == 1) forStatement.addCondition(exp);
                else if (cnt == 2) forStatement.addProcess(exp);
                else throw new InternalError();
            }
        }
        Enviroment.outScope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterContinueStatement(MargatroidParser.ContinueStatementContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitContinueStatement(MargatroidParser.ContinueStatementContext ctx) {
        returnNode.put(ctx, ContinueStatement.getStatement());
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBreakStatement(MargatroidParser.BreakStatementContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBreakStatement(MargatroidParser.BreakStatementContext ctx) {
        returnNode.put(ctx, BreakStatement.getStatement());
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterReturnStatement(MargatroidParser.ReturnStatementContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitReturnStatement(MargatroidParser.ReturnStatementContext ctx) {
        Expression tmp = (Expression)returnNode.get(ctx.expression());
        returnNode.put(ctx, ReturnStatement.getStatement(tmp));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterConstantExpression(MargatroidParser.ConstantExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitConstantExpression(MargatroidParser.ConstantExpressionContext ctx) {
        returnNode.put(ctx, returnNode.get(ctx.constant()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterShiftExpression(MargatroidParser.ShiftExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitShiftExpression(MargatroidParser.ShiftExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        if (ctx.operator.getText().equals("<<"))
            returnNode.put(ctx, BitLeftShiftExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals(">>"))
            returnNode.put(ctx, BitRightShiftExpression.getExpression(x, y));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterAdditiveExpression(MargatroidParser.AdditiveExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitAdditiveExpression(MargatroidParser.AdditiveExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        if (ctx.operator.getText().equals("+"))
            returnNode.put(ctx, AdditionExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals("-"))
            returnNode.put(ctx, MinusExpression.getExpression(x, y));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSubscriptExpression(MargatroidParser.SubscriptExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSubscriptExpression(MargatroidParser.SubscriptExpressionContext ctx) {
        Expression exp1 = (Expression)returnNode.get(ctx.expression(0));
        Expression exp2 = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, SubscriptExpression.getExpression(exp1, exp2));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterRelationalExpression(MargatroidParser.RelationalExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitRelationalExpression(MargatroidParser.RelationalExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        if (ctx.operator.getText().equals("<"))
            returnNode.put(ctx, LessThanExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals(">"))
            returnNode.put(ctx, GreaterThanExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals("<="))
            returnNode.put(ctx, LEQExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals(">="))
            returnNode.put(ctx, GEQExpression.getExpression(x, y));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterInclusiveOrExpression(MargatroidParser.InclusiveOrExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitInclusiveOrExpression(MargatroidParser.InclusiveOrExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, BitOrExpression.getExpression(x, y));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterNewExpression(MargatroidParser.NewExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNewExpression(MargatroidParser.NewExpressionContext ctx) {
        List<Expression> tmp = new ArrayList<>();
        int cnt = 0;
        int last = -1;
        for (ParseTree parseTree : ctx.children) {
            if (parseTree instanceof MargatroidParser.ExpressionContext) {
                if (cnt > 0) throw new CompileError("Wrong on new a array.");
                last = -1;
            }
            if (parseTree instanceof TerminalNode) {
                Token y = ((TerminalNode)parseTree).getSymbol();
                if (y.getText().equals("[")) last = 0;
                if (y.getText().equals("]")) {
                    if (last == 0) cnt = cnt + 1;
                    last = -1;
                }
            }
        }
        ctx.expression().forEach(expressionContext -> {
            Expression x = (Expression)returnNode.get(expressionContext);
            tmp.add(x);
        });
        last = -1;
        for (ParseTree parseTree : ctx.children) {
            if (parseTree instanceof TerminalNode) {
                Token y = ((TerminalNode)parseTree).getSymbol();
                if (y.getText().equals("[")) last = 0;
                if (y.getText().equals("]")) {
                    if (last == 0) tmp.add(null);
                    last = -1;
                }
            }else last = -1;
        }
        Type type = (Type)returnNode.get(ctx.type());
        returnNode.put(ctx, NewExpression.getExpression(type, tmp));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterAssignmentExpression(MargatroidParser.AssignmentExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitAssignmentExpression(MargatroidParser.AssignmentExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, AssignmentExpression.getExpression(x, y));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterMultiplicativeExpression(MargatroidParser.MultiplicativeExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitMultiplicativeExpression(MargatroidParser.MultiplicativeExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        if (ctx.operator.getText().equals("*"))
            returnNode.put(ctx, MultiplyExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals("/"))
            returnNode.put(ctx, DivideExpression.getExpression(x,y));
        else if (ctx.operator.getText().equals("%"))
            returnNode.put(ctx, ModExpression.getExpression(x, y));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterLogicalOrExpression(MargatroidParser.LogicalOrExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitLogicalOrExpression(MargatroidParser.LogicalOrExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, OrExpression.getExpression(x, y));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVariableExpression(MargatroidParser.VariableExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVariableExpression(MargatroidParser.VariableExpressionContext ctx) {
        returnNode.put(ctx, IdentifierExpression.getExpression(ctx.getText()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterAndExpression(MargatroidParser.AndExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitAndExpression(MargatroidParser.AndExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, BitAndExpression.getExpression(x, y));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterExclusiveOrExpression(MargatroidParser.ExclusiveOrExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitExclusiveOrExpression(MargatroidParser.ExclusiveOrExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, BitXorExpression.getExpression(x, y));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterEqualityExpression(MargatroidParser.EqualityExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitEqualityExpression(MargatroidParser.EqualityExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        if (ctx.operator.getText().equals("=="))
            returnNode.put(ctx, EqualToExpression.getExpression(x, y));
        else if (ctx.operator.getText().equals("!="))
            returnNode.put(ctx, NEQExpression.getExpression(x, y));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterLogicalAndExpression(MargatroidParser.LogicalAndExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitLogicalAndExpression(MargatroidParser.LogicalAndExpressionContext ctx) {
        Expression x = (Expression)returnNode.get(ctx.expression(0));
        Expression y = (Expression)returnNode.get(ctx.expression(1));
        returnNode.put(ctx, AndExpression.getExpression(x, y));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterFieldExpression(MargatroidParser.FieldExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFieldExpression(MargatroidParser.FieldExpressionContext ctx) {
        returnNode.put(ctx, FieldExpression.getExpression((Expression)returnNode.get(ctx.expression()), ctx.Identifier().getText()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterFunctionCallExpression(MargatroidParser.FunctionCallExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFunctionCallExpression(MargatroidParser.FunctionCallExpressionContext ctx) {
        Expression function = (Expression)returnNode.get(ctx.expression(0));
        List<Expression> parameters = new ArrayList<>();
        for (int i = 1; i < ctx.expression().size(); ++i) {
            Expression parameter = (Expression)returnNode.get(ctx.expression(i));
            parameters.add(parameter);
        }
        returnNode.put(ctx, FunctionCallExpression.getExpression(function, parameters));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterUnaryExpression(MargatroidParser.UnaryExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitUnaryExpression(MargatroidParser.UnaryExpressionContext ctx) {
        Expression expression = (Expression)returnNode.get(ctx.expression());
        if (ctx.operator.getText().equals("+"))
            returnNode.put(ctx, UnaryPlusExpression.getExpression(expression));
        else if (ctx.operator.getText().equals("-"))
            returnNode.put(ctx, UnaryMinusExpression.getExpression(expression));
        else if (ctx.operator.getText().equals("!"))
            returnNode.put(ctx, NotExpression.getExpression(expression));
        else if (ctx.operator.getText().equals("~"))
            returnNode.put(ctx, BitNotExpression.getExpression(expression));
        else if (ctx.operator.getText().equals("++"))
            returnNode.put(ctx, PrefixIncreaseExpression.getExpression(expression));
        else if (ctx.operator.getText().equals("--"))
            returnNode.put(ctx, PrefixDecreaseExpression.getExpression(expression));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSubExpression(MargatroidParser.SubExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSubExpression(MargatroidParser.SubExpressionContext ctx) {
        returnNode.put(ctx, returnNode.get(ctx.expression()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterPostfixExpression(MargatroidParser.PostfixExpressionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitPostfixExpression(MargatroidParser.PostfixExpressionContext ctx) {
        Expression expression = (Expression)returnNode.get(ctx.expression());
        if (ctx.operator.getText().equals("++"))
            returnNode.put(ctx, PostfixIncreaseExpression.getExpression(expression));
        else if (ctx.operator.getText().equals("--"))
            returnNode.put(ctx, PostfixDecreaseExpression.getExpression(expression));
        else throw new InternalError();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterArrayType(MargatroidParser.ArrayTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitArrayType(MargatroidParser.ArrayTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIntType(MargatroidParser.IntTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIntType(MargatroidParser.IntTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStringType(MargatroidParser.StringTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStringType(MargatroidParser.StringTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVoidType(MargatroidParser.VoidTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVoidType(MargatroidParser.VoidTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBoolType(MargatroidParser.BoolTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBoolType(MargatroidParser.BoolTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterClassType(MargatroidParser.ClassTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitClassType(MargatroidParser.ClassTypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBoolConstant(MargatroidParser.BoolConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBoolConstant(MargatroidParser.BoolConstantContext ctx) {
        returnNode.put(ctx, BoolConstant.getConstant(Boolean.valueOf(ctx.getText())));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIntConstant(MargatroidParser.IntConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIntConstant(MargatroidParser.IntConstantContext ctx) {
        try {
            returnNode.put(ctx, IntConstant.getConstant(Integer.valueOf(ctx.getText())));
        }catch (NumberFormatException e) {
            throw new CompileError("Number Format Wrong.");
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStringConstant(MargatroidParser.StringConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStringConstant(MargatroidParser.StringConstantContext ctx) {
        returnNode.put(ctx, StringConstant.getConstant(ctx.getText().substring(1, ctx.getText().length() - 1)));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterNullConstant(MargatroidParser.NullConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNullConstant(MargatroidParser.NullConstantContext ctx) {
        returnNode.put(ctx, NullConstant.getConstant());
    }

}