grammar Margatroid;

CLASS : 'class';

LB : '{';

RB : '}';

EQUAL : '=';

COMMA : ',';

SEMI : ';';

LM : '[';

RM : ']';

LS : '(';

RS : ')';

program : (classDeclaration | functionDeclaration | variableDeclarationStatement)+;
classDeclaration : CLASS Identifier LB (functionDeclaration | variableDeclarationStatement)* RB;
functionDeclaration	:  type Identifier? LS (type Identifier (COMMA type Identifier)*)? RS compondStatement;
variableDeclarationStatement :  type Identifier (EQUAL expression)? SEMI;

statement	:	compondStatement
			|	expressionStatement
			|	ifStatement
			|	loopStatement
			|	jumpStatement
			|	variableDeclarationStatement;

compondStatement	:	LB (statement)* RB;

expressionStatement	:	(expression)? SEMI;

IF : 'if';

ELSE : 'else';

WHILE : 'while';

FOR : 'for';

ifStatement	:	IF LS expression RS statement (ELSE statement)?;

loopStatement	:	WHILE LS expression RS statement								#whileStatement
				|	FOR LS (expression)? SEMI (expression)? SEMI (expression)? RS statement	#forStatement
				;

CONTINUE : 'continue';

BREAK : 'break';

RETURN : 'return';

jumpStatement	:	CONTINUE SEMI				#continueStatement
				|	BREAK SEMI					#breakStatement
				|	RETURN (expression)? SEMI	#returnStatement
				;


expression  :	constant                                            #constantExpression
			|	Identifier											#variableExpression
			|	LS expression RS                                  #subExpression
			|	expression operator = ('++' | '--')						#postfixExpression
			|	expression LS (expression (COMMA expression)*)? RS  #functionCallExpression
			|   expression LM expression RM						#subscriptExpression
			|   expression '.' Identifier							#fieldExpression
			|	operator = ('+' | '-' | '!' | '~' | '++' | '--') expression     #unaryExpression
			|	'new' type ('[' expression? ']')*             #newExpression
			|	expression operator = ('*' | '/' | '%') expression        #multiplicativeExpression
			|	expression operator = ('+' | '-') expression            #additiveExpression
			|	expression operator = ('<<' | '>>') expression          #shiftExpression
			|	expression operator = ('<' | '>' | '<=' | '>=') expression  #relationalExpression
			|	expression operator = ('==' | '!=') expression          #equalityExpression
			|	expression '&' expression                           #andExpression
			|	expression '^' expression                           #exclusiveOrExpression
			|	expression '|' expression                           #inclusiveOrExpression
			|	expression '&&' expression                          #logicalAndExpression
			|	expression '||' expression                          #logicalOrExpression
			|	<assoc=right> expression '=' expression				#assignmentExpression
			;

Void : 'void';
Int : 'int';
Bool : 'bool';
String : 'string';

type	:	Void		#voidType
		|	Int		#intType
		|	Bool		#boolType
		|	String	#stringType
		|	Identifier	#classType
		|	type '[' ']'	#arrayType
		;


constant    :	('true' | 'false')	#boolConstant
			|	INTEGER				#intConstant
			|	STRING				#stringConstant
			|	'null'				#nullConstant
			;

Identifier	:	[a-zA-Z_][a-zA-Z_0-9]*;

INTEGER	:	[0-9]+;

STRING	:	'\"' CHAR* '\"';

fragment
CHAR	:	~["\\\r\n]
		|	'\\' ['"?abfnrtv\\]
		;

LINECOMMENT :   '//' ~[\r\n]*   ->  skip;

BLOCKCOMMENT	:	'/*' .*? '*/'	->	skip;

WHITESPACE  :   [ \t\r\n]+  ->  skip;