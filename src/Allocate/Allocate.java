package Allocate;

import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Function;

import java.util.*;

public abstract class Allocate {
    public Function function;
    public Map<VirtualRegister, PhysicalRegister> mapping;
    public Allocate(Function function) {
        this.function = function;
        this.mapping = new HashMap<>();
    }
    public Set<PhysicalRegister> getUsedPhysicalRegister() {
        Set<PhysicalRegister> x = new HashSet<PhysicalRegister>();
        for (VirtualRegister virtualRegister : mapping.keySet()) {
            PhysicalRegister tmp = mapping.get(virtualRegister);
            if (tmp != null) x.add(tmp);
        }
        return x;
    }
}