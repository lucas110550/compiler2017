package Allocate.GlobalRegisterAllocator.GraphColoring;

import Allocate.GlobalRegisterAllocator.InterGraph;
import Allocate.PhysicalRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import java.util.*;

public class BruteColoring extends GraphColoring {
    public Set<VirtualRegister> vis;
    public BruteColoring(InterGraph graph) {
        super(graph);
        vis = new HashSet<>();
    }
    public Map<VirtualRegister, PhysicalRegister> work() {
        for (VirtualRegister x : graph.nodes)
            if (!vis.contains(x)) dfs(x);
        return mapping;
    }
    public void dfs(VirtualRegister x) {
        vis.add(x);
        color(x);
        for (VirtualRegister y : graph.ban.get(x))
            if (!vis.contains(y)) dfs(y);
    }
}