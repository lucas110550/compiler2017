package Allocate.GlobalRegisterAllocator.GraphColoring;

import Allocate.GlobalRegisterAllocator.InterGraph;
import Allocate.PhysicalRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import java.util.*;

abstract class GraphColoring {
    public InterGraph graph;
    Map<VirtualRegister, PhysicalRegister> mapping;
    GraphColoring(InterGraph graph) {
        this.graph = graph;
        mapping = new HashMap<>();
    }
    void color(VirtualRegister x) {
        Set<PhysicalRegister> used = new HashSet<PhysicalRegister>();
        for (VirtualRegister y : graph.ban.get(x))
            if (mapping.containsKey(y))
                used.add(mapping.get(y));
        used.add(null);
        for (VirtualRegister y : graph.prefer.get(x)) {
            if (!mapping.containsKey(y)) continue;
            PhysicalRegister tmp = mapping.get(y);
            if (!mapping.containsKey(x) && !used.contains(tmp)) {
                mapping.put(x, tmp);
                break;
            }
        }
        for (PhysicalRegister tmp : InterGraph.color) {
            if (!mapping.containsKey(x) && !used.contains(tmp)) {
                mapping.put(x, tmp);
                break;
            }
        }
		if (!mapping.containsKey(x)) mapping.put(x, null);
    }
}