package Allocate.GlobalRegisterAllocator;

import Allocate.Allocate;
import Allocate.GlobalRegisterAllocator.GraphColoring.BruteColoring;
import ControlFlowGraph.Block;
import ControlFlowGraph.Instruction.BinaryInstruction.AdditionInstruction;
import ControlFlowGraph.Instruction.Instruction;
import ControlFlowGraph.Instruction.MemoryInstruction.MovInstruction;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Front.Function;

import java.util.*;

public class GlobalRegisterAllocator extends Allocate {
    public GlobalRegisterAllocator(Function function) {
        super(function);
        function.graph.rebuild();
        InterGraph graph = new InterGraph();
        for (Block block : function.graph.blocks) {
            for (Instruction instruction : block.instructions) {
                for (VirtualRegister virtualRegister : instruction.UsedRegister())
                    graph.add(virtualRegister);
                for (VirtualRegister virtualRegister : instruction.ExistRegister())
                    graph.add(virtualRegister);
            }
        }
        for (Block block : function.graph.blocks) {
            for (Instruction instruction : block.instructions)
                if (instruction instanceof AdditionInstruction) {
                    AdditionInstruction i = (AdditionInstruction)instruction;
                    if (i.l instanceof VirtualRegister)
                        graph.ban(i.d, (VirtualRegister)i.l);
                    if (i.r instanceof VirtualRegister) graph.ban(i.d, (VirtualRegister)i.r);
                }
        }
        for (Block block : function.graph.blocks) {
            Set<VirtualRegister> live = new HashSet<VirtualRegister>() {{
                block.LiveOut.forEach(this::add);
            }};
            for (int i = block.instructions.size() - 1; i >= 0; i--) {
                Instruction instruction = block.instructions.get(i);
                for (VirtualRegister virtualRegister : instruction.ExistRegister())
                    for (VirtualRegister tmp : live)
                        graph.ban(virtualRegister, tmp);
                instruction.ExistRegister().forEach(live::remove);
                instruction.UsedRegister().forEach(live::add);
            }
        }
        for (Block block : function.graph.blocks)
            for (Instruction instruction : block.instructions)
                if (instruction instanceof MovInstruction) {
                    MovInstruction tmp = (MovInstruction)instruction;
                    if (tmp.s instanceof VirtualRegister) graph.prefer(tmp.d, (VirtualRegister)tmp.s);
                }
        mapping = new BruteColoring(graph).work();
    }
}
