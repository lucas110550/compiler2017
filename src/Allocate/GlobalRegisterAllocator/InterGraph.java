package Allocate.GlobalRegisterAllocator;

import Allocate.PhysicalRegister;
import ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TempRegister;
import ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Translator.NASM.NASMRegister;

import java.util.*;

public class InterGraph {
    public static List<PhysicalRegister> color = new ArrayList<PhysicalRegister>() {{
        add(NASMRegister.r8);
       add(NASMRegister.r9);
       add(NASMRegister.r10);
       add(NASMRegister.r11);
       add(NASMRegister.r14);
       add(NASMRegister.r15);
    }};
    public Set<VirtualRegister> nodes;
    public Map<VirtualRegister, Set<VirtualRegister>> ban;
    public Map<VirtualRegister, Set<VirtualRegister>> prefer;

    InterGraph() {
        nodes = new HashSet<>();
        ban = new HashMap<>();
        prefer = new HashMap<>();
    }
    void add(VirtualRegister x) {
        nodes.add(x);
        ban.put(x, new HashSet<>());
        prefer.put(x, new HashSet<>());
    }
    void ban(VirtualRegister x, VirtualRegister y) {
        if (x == y) return ;
        if (x instanceof TempRegister && y instanceof TempRegister) {
            ban.get(x).add(y);
            ban.get(y).add(x);
        }
    }
    void prefer(VirtualRegister x, VirtualRegister y) {
        if (x == y) return ;
        if (x instanceof TempRegister && y instanceof TempRegister) {
            prefer.get(x).add(y);
            prefer.get(y).add(x);
        }
    }
}