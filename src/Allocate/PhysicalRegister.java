package Allocate;

public abstract class PhysicalRegister {
    public int index;
    public String name;
    public PhysicalRegister(int index, String name) {
        this.index = index;
        this.name = name;
    }
    @Override
    public String toString() { return name; }
}