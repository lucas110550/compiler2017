// Generated from /home/lucas110550/IdeaProjects/Margatroid/src/CST/Parser/Margatroid.g4 by ANTLR 4.6
package CST.Parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MargatroidParser}.
 */
public interface MargatroidListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MargatroidParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MargatroidParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(MargatroidParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(MargatroidParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(MargatroidParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(MargatroidParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#variableDeclarationStatement}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclarationStatement(MargatroidParser.VariableDeclarationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#variableDeclarationStatement}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclarationStatement(MargatroidParser.VariableDeclarationStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(MargatroidParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(MargatroidParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#compondStatement}.
	 * @param ctx the parse tree
	 */
	void enterCompondStatement(MargatroidParser.CompondStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#compondStatement}.
	 * @param ctx the parse tree
	 */
	void exitCompondStatement(MargatroidParser.CompondStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(MargatroidParser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(MargatroidParser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MargatroidParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(MargatroidParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MargatroidParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(MargatroidParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link MargatroidParser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(MargatroidParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link MargatroidParser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(MargatroidParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link MargatroidParser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(MargatroidParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link MargatroidParser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(MargatroidParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link MargatroidParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(MargatroidParser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link MargatroidParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(MargatroidParser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link MargatroidParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(MargatroidParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link MargatroidParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(MargatroidParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link MargatroidParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(MargatroidParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link MargatroidParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(MargatroidParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constantExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpression(MargatroidParser.ConstantExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constantExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpression(MargatroidParser.ConstantExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code shiftExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpression(MargatroidParser.ShiftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code shiftExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpression(MargatroidParser.ShiftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additiveExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(MargatroidParser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additiveExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(MargatroidParser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subscriptExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubscriptExpression(MargatroidParser.SubscriptExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subscriptExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubscriptExpression(MargatroidParser.SubscriptExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpression(MargatroidParser.RelationalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpression(MargatroidParser.RelationalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inclusiveOrExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInclusiveOrExpression(MargatroidParser.InclusiveOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inclusiveOrExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInclusiveOrExpression(MargatroidParser.InclusiveOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNewExpression(MargatroidParser.NewExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNewExpression(MargatroidParser.NewExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignmentExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentExpression(MargatroidParser.AssignmentExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignmentExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentExpression(MargatroidParser.AssignmentExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicativeExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(MargatroidParser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicativeExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(MargatroidParser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logicalOrExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOrExpression(MargatroidParser.LogicalOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logicalOrExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOrExpression(MargatroidParser.LogicalOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVariableExpression(MargatroidParser.VariableExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVariableExpression(MargatroidParser.VariableExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(MargatroidParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(MargatroidParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exclusiveOrExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExclusiveOrExpression(MargatroidParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exclusiveOrExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExclusiveOrExpression(MargatroidParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalityExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpression(MargatroidParser.EqualityExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalityExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpression(MargatroidParser.EqualityExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logicalAndExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalAndExpression(MargatroidParser.LogicalAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logicalAndExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalAndExpression(MargatroidParser.LogicalAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code fieldExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFieldExpression(MargatroidParser.FieldExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code fieldExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFieldExpression(MargatroidParser.FieldExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionCallExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallExpression(MargatroidParser.FunctionCallExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionCallExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallExpression(MargatroidParser.FunctionCallExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpression(MargatroidParser.UnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpression(MargatroidParser.UnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubExpression(MargatroidParser.SubExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubExpression(MargatroidParser.SubExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfixExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExpression(MargatroidParser.PostfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfixExpression}
	 * labeled alternative in {@link MargatroidParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExpression(MargatroidParser.PostfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void enterArrayType(MargatroidParser.ArrayTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void exitArrayType(MargatroidParser.ArrayTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void enterIntType(MargatroidParser.IntTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void exitIntType(MargatroidParser.IntTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void enterStringType(MargatroidParser.StringTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void exitStringType(MargatroidParser.StringTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void enterVoidType(MargatroidParser.VoidTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void exitVoidType(MargatroidParser.VoidTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void enterBoolType(MargatroidParser.BoolTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void exitBoolType(MargatroidParser.BoolTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void enterClassType(MargatroidParser.ClassTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classType}
	 * labeled alternative in {@link MargatroidParser#type}.
	 * @param ctx the parse tree
	 */
	void exitClassType(MargatroidParser.ClassTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterBoolConstant(MargatroidParser.BoolConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitBoolConstant(MargatroidParser.BoolConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterIntConstant(MargatroidParser.IntConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitIntConstant(MargatroidParser.IntConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterStringConstant(MargatroidParser.StringConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitStringConstant(MargatroidParser.StringConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterNullConstant(MargatroidParser.NullConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MargatroidParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitNullConstant(MargatroidParser.NullConstantContext ctx);
}